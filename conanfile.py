from conan import ConanFile

class SpectroHR460Recipe(ConanFile):
    name = "spectrohr460"
    executable = "ds_SpectroHR460"
    version = "1.1.0"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Florent Langlois, Sonia Minolli"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/measureinstruments/spectrohr460.git"
    description = "None"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
