//=============================================================================
// SpectroHR460TypesAndConsts.h
//=============================================================================
// abstraction.......SpectroHR460
// class.............SpectroHR460TypesAndConsts
// original author.... S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _SPECTRO_HR460_TYPES_AND_CONSTS_H_
#define _SPECTRO_HR460_TYPES_AND_CONSTS_H_

#pragma once

//=============================================================================
// DEPENDENCIES
//=============================================================================


namespace SpectroHR460_ns
{

//=============================================================================
// CONSTS
//=============================================================================
//- property fields separator
const std::string KEY_VALUE_SEPARATOR = ":";

//- slit attribute prefix
const std::string SLIT_ATTR_PREFIX = "slitPosition_";

//- Mirror positions
const std::string MIRROR_POS_AXIAL = "AXIAL";
const std::string MIRROR_POS_LATERAL = "LATERAL";
const std::string MIRROR_POS_UNDEF = "UNKNOWN";

//- Default positions at init
const std::string DEFAULT_MIRROR_POS = MIRROR_POS_AXIAL;
const size_t DEFAULT_GRATING_IDX = 1;

//- microns to slit opening conversion factor
const double STEPS_TO_MICRONS_FACTOR = 1.8333333;

//- Default timeout value for spectro task messages (in ms):
#define DEFAULT_CMD_TMO 1000

//=============================================================================
// TYPES
//=============================================================================

// Slit definition
typedef struct SlitDefinition_t
{
  // slit name
  std::string name;

  // slit position
  double position;
} SlitDefinition_t;

// Manager configuration
typedef struct ManagerConfig_t
{
  // host device
  Tango::DeviceImpl * host_device;
  // memorized grating index
  size_t grating_idx;
  // memorized entrance mirror position
  std::string entrance_mirror_pos;
  // memorized exit mirror position
  std::string exit_mirror_pos;
  // init motors command timeout, in s
  yat::uint16 init_motors_tmo;
  // number of connection retries for GPIB command
  yat::uint16 connection_retries;
  // Spectrometer polling period, in ms
  yat::uint16 polling_period;
  // memorized grating motor backlash, in motor steps
  yat::uint16 grating_motor_backlash;
} ManagerConfig_t;

// Spectrometer initialization status
typedef enum
{
  SPECTRO_INIT = 0,             // Tango::INIT
  SPECTRO_MAIN_PROG,            // Tango::INIT
  SPECTRO_INIT_NEARLY,          // Tango::INIT
  SPECTRO_INIT_FINAL,           // Tango::INIT

  SPECTRO_HUNG,                  // Tango::FAULT
  SPECTRO_INIT_ERR,              // Tango::FAULT

  SPECTRO_INIT_MOTORS_NEEDED,    // Tango::DISABLE

  SPECTRO_INIT_MOTORS,           // Tango::RUNNING

  SPECTRO_INIT_DONE              // Tango::STANDBY
} InitStatus_t;

// slit update
typedef struct SlitUpd_t
{
  // slit idx
  size_t idx;

  // slit position
  double position;
} SlitUpd_t;

//=============================================================================
// UTILS
//=============================================================================

//- get stored value in Device property
template <class T>
static T get_value_as_property(Tango::DeviceImpl* dev_p, const std::string& property_name)
  throw (Tango::DevFailed)
{
  if (!Tango::Util::instance()->_UseDb)
  {
    THROW_DEVFAILED(
        "DEVICE_ERROR",
        "NO DB",
        "SpectroHR460::get_value_as_property");
  }

  T value;
  Tango::DbData	dev_prop;
  dev_prop.push_back(Tango::DbDatum(property_name));

  //-	Call database and extract values
  //--------------------------------------------
  try
  {
    dev_p->get_db_device()->get_property(dev_prop);
  }
  catch (Tango::DevFailed &df)
  {
    RETHROW_DEVFAILED(
          df,
          "DEVICE_ERROR",
          std::string(df.errors[0].desc).c_str(),
          "SpectroHR460::get_value_as_property");
  }

  //-	Try to extract saved property from database
  if (dev_prop[0].is_empty() == false)
  {
    dev_prop[0] >> value;
  }
  else
  {
    // no value stored in data base
    THROW_DEVFAILED(
          "DEVICE_ERROR",
          "No value in database",
          "SpectroHR460::get_value_as_property");
  }

  return value;
}

} // namespace SpectroHR460_ns

#endif // _SPECTRO_HR460_TYPES_AND_CONSTS_H_
