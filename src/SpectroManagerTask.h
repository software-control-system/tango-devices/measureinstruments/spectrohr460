#pragma once
//=============================================================================
// SpectroManagerTask.h
//=============================================================================
// abstraction.......SpectroManagerTask for SpectroHR460
// class.............SpectroManagerTask
// original author...S. Minolli - Nexeya
//=============================================================================

#ifndef _SPECTRO_MANAGER_TASK_H
#define _SPECTRO_MANAGER_TASK_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat4tango/DeviceTask.h>
#include "SpectroHR460TypesAndConsts.h"
#include "HardwareInterface.h"
#include <yat/memory/SharedPtr.h>
#include <yat/threading/Pulser.h>

namespace SpectroHR460_ns
{

// ============================================================================
// class: SpectroManagerTask
// description: Spectrometer manager task
// ============================================================================
class SpectroManagerTask : public yat4tango::DeviceTask
{
  public:
    // Constructor
    // @param cfg Manager config
    // @param gratings Grating list
    SpectroManagerTask(ManagerConfig_t cfg, std::map<size_t, std::string> & gratings, std::map<size_t, SlitDefinition_t> & slits);

    // Destructor
    virtual ~SpectroManagerTask(void);

    // Proxy initialization
    // @param gpib_url GPIB device name
    void init_proxy(std::string & gpib_url)
      throw (Tango::DevFailed);
	
    // Start initialization
    void start_init()
      throw (Tango::DevFailed);

    //- gets  state
    Tango::DevState get_state();

    //- gets status
    std::string get_status();


    // getters: #####################
    // get grating position
    // @return Grating position, in motor steps
    yat::int32 get_grating_position()
      throw (Tango::DevFailed);

    // get central wavelength
    // @return Central wavelength, in nm
    double get_central_wavelength()
      throw (Tango::DevFailed);

    // get limit switch reached (or not)
    // @return Limit switch flag (true = limit reached)
    bool get_limit_switch_reached();

    // get current grating index
    // @return Grating index
    size_t get_current_grating_index();

    // get last entrance mirror position
    // @return Mirror position, among [AXIAL, LATERAL]
    std::string get_entrance_mirror_pos ();

    // get last exit mirror position
    // @return Mirror position, among [AXIAL, LATERAL]
    std::string get_exit_mirror_pos ();

    // get specified slit current position
    // @param idx Slit index
    // @return Slit position, in µm
    double get_slit_position(size_t idx)
      throw (Tango::DevFailed);


    // setters: #####################
    // set grating position
    // @param val Position, in motor steps
    void set_grating_position(yat::int32 val)
      throw (Tango::DevFailed);

    // set central wavelength
    // @param val Wavelength value, in nm
    void set_central_wavelength(double val)
      throw (Tango::DevFailed);

    // set grating index
    // @param idx Grating index
    void set_grating(size_t idx)
      throw (Tango::DevFailed);

    // define grating position
    // @param val Grating position, in motor steps
    void define_grating_position(yat::int32 val)
      throw (Tango::DevFailed);

    // define slit position
    // @param idx Slit index
    // @param val Slit position, in µm
    void define_slit_position(size_t idx, double val)
      throw (Tango::DevFailed);

    // set slit position
    // @param idx Slit index
    // @param val Slit position, in µm
    void set_slit_position(size_t idx, double val)
      throw (Tango::DevFailed);


    // commands: ##################
    // init motors
    void init_motors()
      throw (Tango::DevFailed);

    // reset
    void reset()
      throw (Tango::DevFailed);

    // abort movements
    // @param have_to_wait True if have to wait for answer, false otherwise
    void abort(bool have_to_wait = true)
      throw (Tango::DevFailed);

    // set entrance mirror to AXIAL position
    void set_entrance_mirror_axial()
      throw (Tango::DevFailed);

    // set entrance mirror to LATERAL position
    void set_entrance_mirror_lateral()
      throw (Tango::DevFailed);

    // set exit mirror to AXIAL position
    void set_exit_mirror_axial()
      throw (Tango::DevFailed);

    // set exit mirror to LATERAL position
    void set_exit_mirror_lateral()
        throw (Tango::DevFailed);

  protected:
	
    //- process_message (implements yat4tango::DeviceTask pure virtual method)
    virtual void process_message (yat::Message& msg)
      throw (Tango::DevFailed);

  private:
  
    // periodic job (internal)
    void periodic_job_i();

    // internal functions
    void start_spectro_init_process_i();
    void finalize_spectro_init_process_i(bool final_init);
    void start_spectro_main_prog_i();

    yat::int32 get_grating_pos_i()
      throw (Tango::DevFailed);
    double get_slit_position_i(size_t idx)
      throw (Tango::DevFailed);
    void set_grating_position_i(yat::int32 val)
      throw (Tango::DevFailed);
    void set_grating_i(size_t idx)
      throw (Tango::DevFailed);
    void define_grating_position_i(yat::int32 val)
      throw (Tango::DevFailed);
    void define_slit_position_i(SlitUpd_t cfg)
      throw (Tango::DevFailed);
    void set_slit_position_i(SlitUpd_t cfg)
      throw (Tango::DevFailed);
    void init_motors_i()
      throw (Tango::DevFailed);
    void reset_i()
      throw (Tango::DevFailed);
    void abort_i()
      throw (Tango::DevFailed);
    void set_entrance_mirror_axial_i()
      throw (Tango::DevFailed);
    void set_entrance_mirror_lateral_i()
      throw (Tango::DevFailed);
    void set_exit_mirror_axial_i()
      throw (Tango::DevFailed);
    void set_exit_mirror_lateral_i()
        throw (Tango::DevFailed);

    // Callback for end of init motors tmo			
    void init_motors_tmo(yat::Thread::IOArg arg)
      throw (Tango::DevFailed);

    // compose manager status
    std::string compose_mgr_status(std::string main_msg, 
                                   Tango::DevState gpib_device_state, 
                                   std::string gpib_device_status);

    //- manager state
    Tango::DevState m_mgr_state;

    //- manager status
    std::string m_mgr_status;

    //- limits reached flag
    bool m_limits_reached;

    //- error status
    std::string m_error_status;

    //- config
    ManagerConfig_t m_cfg;

    //- init status
    InitStatus_t m_init_status;

    //- grating list
    //- {key, value} where key = grating index, value = grating name
    std::map<size_t, std::string> m_gratings;

    //- slit list
    //- {key, value} where key = slit index, value = slit position in µm
    std::map<size_t, SlitDefinition_t> m_slits;

    //- current grating index
    size_t m_current_grating_idx;
    //- last update date
    std::string m_last_grating_upd_date; // format "JJ-MM-AAAA_HH:MM:SS"

    //- current entrance mirror position
    std::string m_current_entrance_mirror_pos;
    //- last update date
    std::string m_last_entrance_mirror_upd_date; // format "JJ-MM-AAAA_HH:MM:SS"

    //- current exit mirror position
    std::string m_current_exit_mirror_pos;
    //- last update date
    std::string m_last_exit_mirror_upd_date; // format "JJ-MM-AAAA_HH:MM:SS"

    // Pulser for end of init motors
    yat::Pulser * m_end_of_init_motors_pulser;

    //- to avoid race condition between getters & periodic_job
    yat::Mutex m_data_lock;

    //- gpib proxy
    Tango::DeviceProxy * m_gpib_proxy;

   // end of init motors tmo flag
   // do not read anything while init motors running...
   bool m_read_end_of_init_motors;

	//- spectro interface
    YAT_SHARED_PTR(HardwareInterface) m_spectro_itf;
};

} // namespace SpectroHR460_ns

#endif // _SPECTRO_MANAGER_TASK_H
