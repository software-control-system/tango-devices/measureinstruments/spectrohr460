//=============================================================================
// HardwareInterface.h
//=============================================================================
// abstraction.......Implementation of the GPIB interface to access to
//                   the HR460 spectrometer
// class.............HardwareInterface 
// original author...S. MINOLLI - NEXEYA-FRANCE
//=============================================================================
#ifndef _HARDWAREINTERFACE_H_
#define _HARDWAREINTERFACE_H_

//============================================================================
// DEPENDENCIES
//============================================================================
#include <tango.h>
#include "GPIBTypesAndConsts.h"

#include <yat4tango/LogHelper.h>
#include <yat4tango/ExceptionHelper.h>
#include <yat/utils/Logging.h>


namespace SpectroHR460_ns {


// ============================================================================
// class: HardwareInterface
// ============================================================================
class HardwareInterface : public yat4tango::TangoLogAdapter
{

public:

  // Constructor
  // @param host_device Host device
  // @param gpid_proxy GPIB device proxy
  // @param retries_nb Retries number for spectro connection
  // @param GPID device proxy
  // @param number of retries of GPIB requests
  HardwareInterface (Tango::DeviceImpl * host_device, Tango::DeviceProxy * gpid_proxy, size_t retries_nb);

  // Destructor
  virtual ~HardwareInterface ();

  // Gets interface state and status
  // @param [out] status Status
  // @return state
  Tango::DevState get_state_and_status(std::string& status);

  // Requests WHERE AM I and returns answer
  // @return Command answer
  // @exception GPIB device access problem
  WhereAmI_resp_t where_am_i()
    throw (Tango::DevFailed);

  // Requests START MAIN PROG and returns answer
  // @return Command answer
  // @exception GPIB device access problem
  StartMainProg_resp_t start_main_prog()
    throw (Tango::DevFailed);

  // Requests MOTORS INIT
  // @exception GPIB device access problem
  void motors_init()
    throw (Tango::DevFailed);

  // Gets MOTORS INIT answer
  // @return answer
  // @exception GPIB device access problem
  MotorsInit_resp_t get_end_of_init_motors()
    throw (Tango::DevFailed);

  // Requests REBOOT IF HUNG
  // @exception GPIB device access problem
  void reboot()
    throw (Tango::DevFailed);

  // Gets GRATING SLITS status
  // @return status
  // @exception GPIB device access problem
  GratingSlitStatus_resp_t get_grating_slits_status()
    throw (Tango::DevFailed);

  // Gets ACCESSORIES status
  // @return status
  // @exception GPIB device access problem
  AccessoriesStatus_resp_t get_accessories_status()
    throw (Tango::DevFailed);

  // Gets GRATING LIMIT status
  // @return status
  // @exception GPIB device access problem
  GratingLimitStatus_resp_t get_grating_limit_status()
    throw (Tango::DevFailed);

  // Requests GRATING SLITS STOP
  // @return cmd ack
  // @exception GPIB device access problem
  SimpleCommand_resp_t grating_slits_stop()
    throw (Tango::DevFailed);

  // Requests GRATING RELATIVE MOVE
  // @param steps Relative move, in motor steps
  // @return cmd ack
  // @exception GPIB device access problem
  SimpleCommand_resp_t grating_relative_move(yat::int32 steps)
    throw (Tango::DevFailed);

  // Requests GRATING SET POSITION
  // @param steps Position, in motor steps
  // @return cmd ack
  // @exception GPIB device access problem
  SimpleCommand_resp_t grating_set_position(yat::int32 steps)
    throw (Tango::DevFailed);

  // Gets GRATING POSITION
  // @param [out] pos Current position, in motor steps
  // @return cmd ack
  // @exception GPIB device access problem
  SimpleCommand_resp_t get_grating_position(yat::int32 & pos)
    throw (Tango::DevFailed);

  // Requests SLIT SET POSITION
  // @param idx Slit index
  // @param steps Position, in motor steps
  // @return cmd ack
  // @exception GPIB device access problem
  SimpleCommand_resp_t slit_set_position(size_t idx, yat::int32 steps)
    throw (Tango::DevFailed);

  // Gets SLIT POSITION
  // @param idx Slit index
  // @param [out] pos Current position, in motor steps
  // @return cmd ack
  // @exception GPIB device access problem
  SimpleCommand_resp_t get_slit_position(size_t idx, yat::int32 & pos)
    throw (Tango::DevFailed);

  // Requests SLIT RELATIVE MOVE
  // @param idx Slit index
  // @param steps Relative move, in motor steps
  // @return cmd ack
  // @exception GPIB device access problem
  SimpleCommand_resp_t slit_relative_move(size_t idx,  yat::int32 steps)
    throw (Tango::DevFailed);

  // Requests TURRET MOVE TO POSITION
  // @param idx Position index [0, 1]
  // @return cmd ack
  // @exception GPIB device access problem
  SimpleCommand_resp_t turret_move_to_position(size_t idx)
    throw (Tango::DevFailed);

  // Requests ENTRANCE MIRROR MOVE TO LATERAL
  // @return cmd ack
  // @exception GPIB device access problem
  SimpleCommand_resp_t entrance_mirror_move_lateral()
    throw (Tango::DevFailed);

  // Requests ENTRANCE MIRROR MOVE TO AXIAL
  // @return cmd ack
  // @exception GPIB device access problem
  SimpleCommand_resp_t entrance_mirror_move_axial()
    throw (Tango::DevFailed);

  // Requests EXIT MIRROR MOVE TO LATERAL
  // @return cmd ack
  // @exception GPIB device access problem
  SimpleCommand_resp_t exit_mirror_move_lateral()
    throw (Tango::DevFailed);

  // Requests EXIT MIRROR MOVE TO AXIAL
  // @return cmd ack
  // @exception GPIB device access problem
  SimpleCommand_resp_t exit_mirror_move_axial()
    throw (Tango::DevFailed);

private:

  //- Reads a GPIB answer
  //- @exception GPIB device access problem
  std::string read_answer()
    throw (Tango::DevFailed);

  //- number of retries for GPIB requests
  size_t m_retries_number;
  
  //- Gpib device proxy
  Tango::DeviceProxy * m_gpib_proxy;

};

}
#endif //_HARDWAREINTERFACE_H_
