//=============================================================================
// GPIBTypesAndConsts.h
//=============================================================================
// abstraction.......SpectroHR460
// class.............GPIBTypesAndConsts
// original author.... S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _GPIB_TYPES_AND_CONSTS_H_
#define _GPIB_TYPES_AND_CONSTS_H_

#pragma once

//=============================================================================
// DEPENDENCIES
//=============================================================================


namespace SpectroHR460_ns
{

//=============================================================================
// GPIB COMMANDS
//=============================================================================
//- common commands
const std::string GPIB_CMD_END = "\r";
const char GPIB_RESP_CMD_ACK = 'o'; // ack command

//- WhereAmI
#define GPIB_WHERE_AM_I_CMD " " // request
const char GPIB_WHERE_AM_I_RESP_B = 'B'; // answer ok, boot program
const char GPIB_WHERE_AM_I_RESP_S = '*'; // answer ok, autobauding 1st time
const char GPIB_WHERE_AM_I_RESP_F = 'f'; // answer ok, main prog running (if init from boot)
const char GPIB_WHERE_AM_I_RESP_F2 = 'F'; // answer ok, main prog running (if init motors done)

//- StartMainProgram
#define GPIB_START_MAIN_PROG_CMD "O2000"
const char GPIB_START_MAIN_PROG_RESP = '*'; // answer ok

//- Motorsinit
#define GPIB_MOTORS_INIT_CMD "A" // request
// answer = cmd ack

//- RebootIfHung
#define GPIB_REBOOT_CMD "\xDE" // request
// no answer

//- GratingSlitsGetStatus
#define GPIB_GRAT_SLIT_STATUS_CMD "E" // request
const std::string GPIB_GRAT_SLIT_STATUS_RESP_BUSY = "oq"; // answer ok & busy 
const std::string GPIB_GRAT_SLIT_STATUS_RESP_STDB = "oz"; // answer ok & standby

//- AccessoriesGetStatus
#define GPIB_ACCESSORIES_STATUS_CMD "l" // request
const std::string GPIB_ACCESSORIES_STATUS_RESP_BUSY = "oq"; // answer ok & busy 
const std::string GPIB_ACCESSORIES_STATUS_RESP_STDB = "oz"; // answer ok & standby

//- GratingSlitsStop
#define GPIB_STOP_CMD "L" // request
// answer = cmd ack

//- GratingRelativeMove
#define GPIB_GRATING_MOVE_CMD "F0" // request
// answer = cmd ack

//- GratingGetPosition
#define GPIB_GRATING_GET_POS_CMD "H0" // request
const char GPIB_GRATING_GET_POS_RESP_A = GPIB_RESP_CMD_ACK; // answerA ok

//- GratingSetPosition
#define GPIB_GRATING_SET_POS_CMD "G0" // request
// answer = cmd ack

//- GratingGetLimitStatus
#define GPIB_GRATING_GET_LIMIT_STATUS_CMD "K" // request
const std::string GPIB_GRATING_GET_LIMIT_STATUS_RESP_NOT_REACHED = "o0"; // answer ok & limits not reached

//- SlitSetPosition
#define GPIB_SLIT_SET_POS_CMD "i0" // request
// answer = cmd ack

//- SlitGetPosition
#define GPIB_SLIT_GET_POS_CMD "j0" // request
// answer = cmd ack

//- SlitRelativeMove
#define GPIB_SLIT_MOVE_CMD "k0" // request
// answer = cmd ack

//- TurretMoveToPosition0
#define GPIB_TURRET_MOVE_0_CMD "a0" // request
// answer = cmd ack

//- TurretMoveToPosition1
#define GPIB_TURRET_MOVE_1_CMD "b0" // request
// answer = cmd ack

//- EntranceMirrorMoveToLateral
#define GPIB_ENTRANCE_MIRROR_MOVE_LAT_CMD "c0" // request
// answer = cmd ack

//- EntranceMirrorMoveToAxial
#define GPIB_ENTRANCE_MIRROR_MOVE_AX_CMD "d0" // request
// answer = cmd ack

//- ExitMirrorMoveToLateral
#define GPIB_EXIT_MIRROR_MOVE_LAT_CMD "e0" // request
// answer = cmd ack

//- ExitMirrorMoveToAxial
#define GPIB_EXIT_MIRROR_MOVE_AX_CMD "f0" // request
// answer = cmd ack


//=============================================================================
// TYPES
//=============================================================================

// Where am I command possible answers
typedef enum
{
  WHERE_AM_I_UNKNOWN = -1,
  WHERE_AM_I_DONE = 0,
  WHERE_AM_I_CONT = 1,
  WHERE_AM_I_ERR = 2,
  WHERE_AM_I_INIT_MOTORS_DONE = 3
} WhereAmI_resp_t;

// Start Main program command possible answers
typedef enum
{
  START_MAIN_PROG_UNKNOWN = -1,
  START_MAIN_PROG_DONE = 0,
  START_MAIN_PROG_ERR = 1
} StartMainProg_resp_t;

// Motors init command possible answers
typedef enum
{
  MOTORS_INIT_UNKNOWN = -1,
  MOTORS_INIT_DONE = 0,
  MOTORS_INIT_CONT = 1,
  MOTORS_INIT_ERR = 2
} MotorsInit_resp_t;

// Grating slits status
typedef enum
{
  GRATING_SLIT_UNKNOWN = -1,
  GRATING_SLIT_STANDBY = 0,
  GRATING_SLIT_BUSY = 1,
  GRATING_SLIT_ERR = 2
} GratingSlitStatus_resp_t;

// Accessories status
typedef enum
{
  ACCESSORIES_UNKNOWN = -1,
  ACCESSORIES_STANDBY = 0,
  ACCESSORIES_BUSY = 1,
  ACCESSORIES_ERR = 2
} AccessoriesStatus_resp_t;

// Grating limit status
typedef enum
{
  GRATING_LIMIT_UNKNOWN = -1,
  GRATING_LIMIT_NOT_REACHED = 0,
  GRATING_LIMIT_REACHED = 1,
  GRATING_LIMIT_ERR = 2
} GratingLimitStatus_resp_t;

// Simple command
typedef enum
{
  CMD_ACK_UNKNOWN = -1,
  CMD_ACK_OK = 0,
  CMD_ACK_ERR = 1
} SimpleCommand_resp_t;

} // namespace SpectroHR460_ns

#endif // _GPIB_TYPES_AND_CONSTS_H_
