//=============================================================================
// SpectroManagerTask.cpp
//=============================================================================
// abstraction.......SpectroManagerTask for Spectrometer HR460 device
// class.............SpectroManagerTask
// original author...S. Minolli - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "SpectroManagerTask.h"
#include <yat/utils/XString.h>
#include <yat/time/Time.h>

// ============================================================================
// SOME USER DEFINED MESSAGES FOR THE CountingManager task
// ============================================================================
#define INIT_START (yat::FIRST_USER_MSG + 1001)
#define INIT_FINAL (yat::FIRST_USER_MSG + 1002)
#define INIT_MAIN_PROG (yat::FIRST_USER_MSG + 1003)
#define INIT_FINAL_NEARLY (yat::FIRST_USER_MSG + 1004)

#define GET_GRATING_POS (yat::FIRST_USER_MSG + 1005)
#define GET_SLIT_POS (yat::FIRST_USER_MSG + 1006)

#define SET_GRATING_POS (yat::FIRST_USER_MSG + 1010)
#define SET_GRATING_IDX (yat::FIRST_USER_MSG + 1011)
#define SET_SLIT_POS (yat::FIRST_USER_MSG + 1012)

#define DEFINE_GRATING_POS (yat::FIRST_USER_MSG + 1015)
#define DEFINE_SLIT_POS (yat::FIRST_USER_MSG + 1016)

#define CMD_INIT_MOTORS (yat::FIRST_USER_MSG + 1020)
#define CMD_RESET (yat::FIRST_USER_MSG + 1021)
#define CMD_ABORT (yat::FIRST_USER_MSG + 1022)
#define CMD_ENTRANCE_MIRROR_AXIAL (yat::FIRST_USER_MSG + 1023)
#define CMD_ENTRANCE_MIRROR_LATERAL (yat::FIRST_USER_MSG + 1024)
#define CMD_EXIT_MIRROR_AXIAL (yat::FIRST_USER_MSG + 1025)
#define CMD_EXIT_MIRROR_LATERAL (yat::FIRST_USER_MSG + 1026)
#define END_OF_INIT_MOTORS_TMO (yat::FIRST_USER_MSG + 1027)



namespace SpectroHR460_ns
{

//=============================================================================
//- Check Interface class
//=============================================================================
#define CHECK_HW_PTR \
if (!m_spectro_itf) \
{ \
    THROW_DEVFAILED("INTERNAL_ERROR", \
                    "Request aborted - the hardware interface pointer isn't properly initialized!", \
                    "SpectroManagerTask::check_hw_ptr"); \
}

// ============================================================================
// SpectroManagerTask::SpectroManagerTask ()
// ============================================================================ 
SpectroManagerTask::SpectroManagerTask(ManagerConfig_t cfg, std::map<size_t, std::string> & gratings, 
                                       std::map<size_t, SlitDefinition_t> & slits)
: yat4tango::DeviceTask(cfg.host_device),
 m_cfg(cfg),
 m_gratings(gratings),
 m_slits(slits),
 m_current_grating_idx(cfg.grating_idx),
 m_current_entrance_mirror_pos(cfg.entrance_mirror_pos),
 m_current_exit_mirror_pos(cfg.exit_mirror_pos)
{
  //- No periodic job allowed yet
  enable_periodic_msg(false);

  // init members
  m_mgr_state = Tango::INIT;
  m_mgr_status = "task initializing";
  m_init_status = SPECTRO_INIT;
  m_limits_reached = false;
  m_error_status = "";
  m_read_end_of_init_motors = false;

  m_gpib_proxy = NULL;
  m_spectro_itf = NULL;
  m_end_of_init_motors_pulser = NULL;

  m_last_grating_upd_date = "none";
  m_last_entrance_mirror_upd_date = "none";
  m_last_exit_mirror_upd_date = "none";
}

// ============================================================================
// SpectroManagerTask::~SpectroManagerTask ()
// ============================================================================ 
SpectroManagerTask::~SpectroManagerTask()
{
  //- No more periodic job allowed
  enable_periodic_msg(false);

  m_spectro_itf.reset();
  if (m_gpib_proxy)
  {
    delete m_gpib_proxy;
    m_gpib_proxy = NULL;
  }
  if (m_end_of_init_motors_pulser)
  {
    m_end_of_init_motors_pulser->stop();
    delete m_end_of_init_motors_pulser;
    m_end_of_init_motors_pulser = NULL;
  }
  m_gratings.clear();
}

// ============================================================================
// SpectroManagerTask::init_proxy ()
// ============================================================================ 
void SpectroManagerTask::init_proxy(std::string & gpib_url)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "SpectroManagerTask::init_proxy() entering..." << std::endl;
  
  if (gpib_url.empty())
  {
    THROW_DEVFAILED("CONFIGURATION_ERROR",
          "GPIB URL is empty!",
          "SpectroManagerTask::init_proxy");
  }

  // create gpib proxy
  m_gpib_proxy = new Tango::DeviceProxy(gpib_url);
}

// ============================================================================
// SpectroManagerTask::start_init ()
// ============================================================================ 
void SpectroManagerTask::start_init()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "SpectroManagerTask::start_init() entering..." << std::endl;
  
  if (!m_gpib_proxy)
  {
    THROW_DEVFAILED("INTERNAL_ERROR",
          "Cannot start spectro task: GPIB proxy ptr is null!",
          "SpectroManagerTask::start_init");
  }

  // create HW interface
  m_spectro_itf.reset(new HardwareInterface(m_cfg.host_device, m_gpib_proxy, m_cfg.connection_retries));

  // start init process
  yat::Message * msg = yat::Message::allocate(INIT_START, DEFAULT_MSG_PRIORITY, true);
  this->post(msg);

  //- Setting the polling period of the current task
  set_periodic_msg_period((size_t)m_cfg.polling_period); // ms

  //- Start the periodic msg
  enable_periodic_msg(true);
}
	  
// ============================================================================
// SpectroManagerTask::get_state ()
// ============================================================================ 
Tango::DevState SpectroManagerTask::get_state()
{
  yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
  return m_mgr_state;
}
	  
// ============================================================================
// SpectroManagerTask::get_status ()
// ============================================================================ 
std::string SpectroManagerTask::get_status()
{
  yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
  return m_mgr_status;
}

// ============================================================================
// SpectroManagerTask::get_grating_position ()
// ============================================================================ 
yat::int32 SpectroManagerTask::get_grating_position()
  throw (Tango::DevFailed)
{
  yat::int32 pos;
  yat::Message * msg = yat::Message::allocate(GET_GRATING_POS, DEFAULT_MSG_PRIORITY, true);

  try
  {
    this->wait_msg_handled(msg->duplicate(), DEFAULT_CMD_TMO);
  }
  catch (...)
  {
    msg->release();
    throw;
  }

  pos = msg->get_data<yat::int32>();
  msg->release();

  return pos;
}

// ============================================================================
// SpectroManagerTask::get_grating_pos_i ()
// ============================================================================ 
yat::int32 SpectroManagerTask::get_grating_pos_i()
  throw (Tango::DevFailed)
{
  CHECK_HW_PTR;

  yat::int32 pos;
  SimpleCommand_resp_t resp = m_spectro_itf->get_grating_position(pos);
  
  if (resp != CMD_ACK_OK)
  {
    THROW_DEVFAILED("COMMUNICATION_ERROR",
          "Reading error!",
          "SpectroManagerTask::get_grating_pos_i");
  }

  return pos;
}

// ============================================================================
// SpectroManagerTask::get_central_wavelength ()
// ============================================================================ 
double SpectroManagerTask::get_central_wavelength()
  throw (Tango::DevFailed)
{
  double wl = yat::IEEE_NAN;

  // get grating position in motor steps
  yat::int32 pos;
  yat::Message * msg = yat::Message::allocate(GET_GRATING_POS, DEFAULT_MSG_PRIORITY, true);

  try
  {
    this->wait_msg_handled(msg->duplicate(), DEFAULT_CMD_TMO);
  }
  catch (...)
  {
    msg->release();
    throw;
  }

  pos = msg->get_data<yat::int32>();
  msg->release();

  DEBUG_STREAM << " SpectroManagerTask::get_central_wavelength() - grating position in motor steps = " << pos << std::endl;
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
    // conversion to wavelength: lambda = motor pos / 160 * 1200 / current grating density
    wl = (double) (pos / 160.0 * 1200.0 / yat::XString<double>::to_num(m_gratings[m_current_grating_idx]));
  }

  return wl;
}

// ============================================================================
// SpectroManagerTask::get_limit_switch_reached ()
// ============================================================================ 
bool SpectroManagerTask::get_limit_switch_reached()
{
  yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
  return m_limits_reached;
}

// ============================================================================
// SpectroManagerTask::get_grating_position ()
// ============================================================================ 
 size_t SpectroManagerTask::get_current_grating_index()
{
   yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
   return m_current_grating_idx;
}

 // ============================================================================
 // SpectroManagerTask::get_entrance_mirror_pos ()
 // ============================================================================ 
 std::string SpectroManagerTask::get_entrance_mirror_pos()
 {
   yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
   return m_current_entrance_mirror_pos;
 }

 // ============================================================================
 // SpectroManagerTask::get_exit_mirror_pos ()
 // ============================================================================ 
 std::string SpectroManagerTask::get_exit_mirror_pos()
 {
   yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
   return m_current_exit_mirror_pos;
 }

// ============================================================================
// SpectroManagerTask::get_slit_position ()
// ============================================================================ 
double SpectroManagerTask::get_slit_position(size_t idx)
  throw (Tango::DevFailed)
{
  double pos;
  yat::Message * msg = yat::Message::allocate(GET_SLIT_POS, DEFAULT_MSG_PRIORITY, true);
  msg->attach_data(idx);

  try
  {
    this->wait_msg_handled(msg->duplicate(), DEFAULT_CMD_TMO);
  }
  catch (...)
  {
    msg->release();
    throw;
  }

  pos = msg->get_data<double>();
  msg->release();

  return pos;
}

// ============================================================================
// SpectroManagerTask::get_slit_position_i ()
// ============================================================================ 
double SpectroManagerTask::get_slit_position_i(size_t idx)
  throw (Tango::DevFailed)
{
  CHECK_HW_PTR;

  yat::int32 steps;
  SimpleCommand_resp_t resp = m_spectro_itf->get_slit_position(idx, steps);

  if (resp != CMD_ACK_OK)
  {
    THROW_DEVFAILED("COMMUNICATION_ERROR",
            "Reading error!",
            "SpectroManagerTask::get_slit_position_i");
  }

  // conversion from motor steps to microns
  double pos = STEPS_TO_MICRONS_FACTOR * steps;
  return pos;
}

// ============================================================================
// SpectroManagerTask::set_grating_position ()
// ============================================================================ 
void SpectroManagerTask::set_grating_position(yat::int32 val)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "SpectroManagerTask::set_grating_position() entering... " << endl;
  DEBUG_STREAM << "grating pos = " << val << " steps" << endl;

  yat::Message * msg = yat::Message::allocate(SET_GRATING_POS, DEFAULT_MSG_PRIORITY, true);

  msg->attach_data(val);

  this->wait_msg_handled(msg, DEFAULT_CMD_TMO);
}

// ============================================================================
// SpectroManagerTask::set_grating_position_i ()
// ============================================================================ 
void SpectroManagerTask::set_grating_position_i(yat::int32 val)
  throw (Tango::DevFailed)
{
  CHECK_HW_PTR;
 
  // get current grating position 
  yat::int32 current_pos;
  SimpleCommand_resp_t resp = m_spectro_itf->get_grating_position(current_pos);

  if (resp != CMD_ACK_OK)
  {
    THROW_DEVFAILED("COMMUNICATION_ERROR",
          "Reading error (current grating position)!",
          "SpectroManagerTask::set_grating_position_i");
  }

  // compute relative move
  yat::int32 delta_steps = val - current_pos;

  // apply backlash: if delta steps < 0, delta steps = delta steps – backlash
  if (delta_steps < 0)
  {
      delta_steps -= m_cfg.grating_motor_backlash;
  }

  resp = m_spectro_itf->grating_relative_move(delta_steps);

  if (resp != CMD_ACK_OK)
  {
    THROW_DEVFAILED("COMMUNICATION_ERROR",
            "Reading error!",
            "SpectroManagerTask::set_grating_position_i");
  }
}

// ============================================================================
// SpectroManagerTask::set_central_wavelength ()
// ============================================================================ 
void SpectroManagerTask::set_central_wavelength(double val)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "SpectroManagerTask::set_central_wavelength() entering... " << endl;
  DEBUG_STREAM << "central wavelength = " << val << " nm" << endl;

  // conversion in motor steps: wl x 160 x density / 1200
  yat::int32 setpoint_in_steps;
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
    setpoint_in_steps = 
      (yat::int32)(val * 160.0 * yat::XString<double>::to_num(m_gratings[m_current_grating_idx]) / 1200.0);
  }
  
  yat::Message * msg = yat::Message::allocate(SET_GRATING_POS, DEFAULT_MSG_PRIORITY, true);
  msg->attach_data(setpoint_in_steps);

  this->wait_msg_handled(msg, DEFAULT_CMD_TMO);
}

// ============================================================================
// SpectroManagerTask::set_grating ()
// ============================================================================ 
void SpectroManagerTask::set_grating(size_t idx)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "SpectroManagerTask::set_grating() entering... " << endl;
  DEBUG_STREAM << "grating index = " << idx << " steps" << endl;

  yat::Message * msg = yat::Message::allocate(SET_GRATING_IDX, DEFAULT_MSG_PRIORITY, true);

  msg->attach_data(idx);

  this->wait_msg_handled(msg, DEFAULT_CMD_TMO);
}

// ============================================================================
// SpectroManagerTask::set_grating_i ()
// ============================================================================ 
void SpectroManagerTask::set_grating_i(size_t idx)
  throw (Tango::DevFailed)
{
  CHECK_HW_PTR;

  SimpleCommand_resp_t resp = m_spectro_itf->turret_move_to_position(idx);

  if (resp != CMD_ACK_OK)
  {
    THROW_DEVFAILED("COMMUNICATION_ERROR",
            "Reading error!",
            "SpectroManagerTask::set_grating_i");
  }

  // update last command value & date
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
    m_current_grating_idx = idx;
  }
  yat::CurrentTime now;
  m_last_grating_upd_date = yat::String::str_format("%02d-%02d-%04d_%02d:%02d:%02d"
      , now.day(), now.month(), now.year()
      , now.hour(), now.minute(), (int)now.second());
}

// ============================================================================
// SpectroManagerTask::define_grating_position ()
// ============================================================================ 
void SpectroManagerTask::define_grating_position(yat::int32 val)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "SpectroManagerTask::define_grating_position() entering... " << endl;
  DEBUG_STREAM << "grating pos = " << val << " steps" << endl;

  yat::Message * msg = yat::Message::allocate(DEFINE_GRATING_POS, DEFAULT_MSG_PRIORITY, true);

  msg->attach_data(val);

  this->wait_msg_handled(msg, DEFAULT_CMD_TMO);
}

// ============================================================================
// SpectroManagerTask::define_grating_position_i ()
// ============================================================================ 
void SpectroManagerTask::define_grating_position_i(yat::int32 val)
  throw (Tango::DevFailed)
{
  CHECK_HW_PTR;

  SimpleCommand_resp_t resp = m_spectro_itf->grating_set_position(val);

  if (resp != CMD_ACK_OK)
  {
    THROW_DEVFAILED("COMMUNICATION_ERROR",
          "Reading error!",
          "SpectroManagerTask::define_grating_position_i");
  }
}

// ============================================================================
// SpectroManagerTask::define_slit_position ()
// ============================================================================ 
void SpectroManagerTask::define_slit_position(size_t idx, double val)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "SpectroManagerTask::define_slit_position() entering... " << endl;
  DEBUG_STREAM << "slit idx = " << idx << " - pos = " << val << " um" << endl;

  yat::Message * msg = yat::Message::allocate(DEFINE_SLIT_POS, DEFAULT_MSG_PRIORITY, true);

  SlitUpd_t cfg;
  cfg.idx = idx;
  cfg.position = val;
  msg->attach_data(cfg);

  this->wait_msg_handled(msg, DEFAULT_CMD_TMO);
}

// ============================================================================
// SpectroManagerTask::define_slit_position_i ()
// ============================================================================ 
void SpectroManagerTask::define_slit_position_i(SlitUpd_t cfg)
  throw (Tango::DevFailed)
{
  CHECK_HW_PTR;

  // conversion from microns to motor steps
  yat::int32 steps = (yat::int32)(cfg.position / STEPS_TO_MICRONS_FACTOR);

  SimpleCommand_resp_t resp = m_spectro_itf->slit_set_position(cfg.idx, steps);

  if (resp != CMD_ACK_OK)
  {
    THROW_DEVFAILED("COMMUNICATION_ERROR",
            "Reading error!",
            "SpectroManagerTask::define_slit_position_i");
  }
}

// ============================================================================
// SpectroManagerTask::set_slit_position ()
// ============================================================================ 
void SpectroManagerTask::set_slit_position(size_t idx, double val)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "SpectroManagerTask::set_slit_position() entering... " << endl;
  DEBUG_STREAM << "slit idx = " << idx << " - pos = " << val << " um" << endl;

  yat::Message * msg = yat::Message::allocate(SET_SLIT_POS, DEFAULT_MSG_PRIORITY, true);

  SlitUpd_t cfg;
  cfg.idx = idx;
  cfg.position = val;
  msg->attach_data(cfg);

  this->wait_msg_handled(msg, DEFAULT_CMD_TMO);
}

// ============================================================================
// SpectroManagerTask::set_slit_position_i ()
// ============================================================================ 
void SpectroManagerTask::set_slit_position_i(SlitUpd_t cfg)
  throw (Tango::DevFailed)
{
  CHECK_HW_PTR;

  // get current slit position
  yat::int32 current_steps;
  SimpleCommand_resp_t resp = m_spectro_itf->get_slit_position(cfg.idx, current_steps);

  if (resp != CMD_ACK_OK)
  {
    THROW_DEVFAILED("COMMUNICATION_ERROR",
          "Reading error (current slit position)!",
          "SpectroManagerTask::set_slit_position_i");
  }

  // conversion from microns to motor steps
  yat::int32 setpoint_steps = (yat::int32)(cfg.position / STEPS_TO_MICRONS_FACTOR);

  // compute relative move in motor steps
  yat::int32 delta_steps = setpoint_steps - current_steps;

  resp = m_spectro_itf->slit_relative_move(cfg.idx, delta_steps);

  if (resp != CMD_ACK_OK)
  {
    THROW_DEVFAILED("COMMUNICATION_ERROR",
            "Reading error!",
            "SpectroManagerTask::set_slit_position_i");
  }
}

// ============================================================================
// SpectroManagerTask::init_motors ()
// ============================================================================ 
void SpectroManagerTask::init_motors()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "SpectroManagerTask::init_motors() entering... " << endl;

  yat::Message * msg = yat::Message::allocate(CMD_INIT_MOTORS, DEFAULT_MSG_PRIORITY, true);

  this->wait_msg_handled(msg, DEFAULT_CMD_TMO);

  // arm a timer to avoid reading while init motors running
  // at the end of tmo, init motors should return 'done'
  // instanciate the associated pulser
  yat::Pulser::Config l_end_of_init_motors_cfg;
  l_end_of_init_motors_cfg.period_in_msecs = (size_t)(m_cfg.init_motors_tmo * 1000);
  l_end_of_init_motors_cfg.num_pulses = 1;
  l_end_of_init_motors_cfg.callback = yat::PulserCallback::instanciate(*this, &SpectroManagerTask::init_motors_tmo);
  l_end_of_init_motors_cfg.user_data = 0;

  m_end_of_init_motors_pulser = new yat::Pulser(l_end_of_init_motors_cfg);

  if (!m_end_of_init_motors_pulser)
  {
    THROW_DEVFAILED("INTERNAL_ERROR",
          "Failed to launch tmo pulser!",
          "SpectroManagerTask::init_motors");
  }

  m_end_of_init_motors_pulser->start();
}

// ============================================================================
// SpectroManagerTask::init_motors_i ()
// ============================================================================ 
void SpectroManagerTask::init_motors_i()
  throw (Tango::DevFailed)
{
  CHECK_HW_PTR;

  m_spectro_itf->motors_init();

  m_init_status = SPECTRO_INIT_MOTORS;
  // disable reading
  m_read_end_of_init_motors = false;

  // force state to RUNNING until next periodic_job (?)
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
    m_mgr_state = Tango::RUNNING;
  }
}

// ============================================================================
// SpectroManagerTask::reset ()
// ============================================================================ 
void SpectroManagerTask::reset()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "SpectroManagerTask::reset() entering... " << endl;

  yat::Message * msg = yat::Message::allocate(CMD_RESET, DEFAULT_MSG_PRIORITY, true);

  this->wait_msg_handled(msg, DEFAULT_CMD_TMO);
}

// ============================================================================
// SpectroManagerTask::reset_i ()
// ============================================================================ 
void SpectroManagerTask::reset_i()
  throw (Tango::DevFailed)
{
  CHECK_HW_PTR;

  m_spectro_itf->reboot();
}

// ============================================================================
// SpectroManagerTask::abort ()
// ============================================================================ 
void SpectroManagerTask::abort(bool have_to_wait)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "SpectroManagerTask::abort() entering... " << endl;

  yat::Message * msg = yat::Message::allocate(CMD_ABORT, DEFAULT_MSG_PRIORITY, true);

  if (have_to_wait)
    this->wait_msg_handled(msg, DEFAULT_CMD_TMO);
  else
    this->post(msg);
}

// ============================================================================
// SpectroManagerTask::abort_i ()
// ============================================================================ 
void SpectroManagerTask::abort_i()
  throw (Tango::DevFailed)
{
  CHECK_HW_PTR;

  SimpleCommand_resp_t resp = m_spectro_itf->grating_slits_stop();

  if (resp != CMD_ACK_OK)
  {
    THROW_DEVFAILED("COMMUNICATION_ERROR",
            "Reading error!",
            "SpectroManagerTask::abort_i");
  }
}

// ============================================================================
// SpectroManagerTask::set_entrance_mirror_axial ()
// ============================================================================ 
void SpectroManagerTask::set_entrance_mirror_axial()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "SpectroManagerTask::set_entrance_mirror_axial() entering... " << endl;

  yat::Message * msg = yat::Message::allocate(CMD_ENTRANCE_MIRROR_AXIAL, DEFAULT_MSG_PRIORITY, true);

  this->wait_msg_handled(msg, DEFAULT_CMD_TMO);
}

// ============================================================================
// SpectroManagerTask::set_entrance_mirror_axial_i ()
// ============================================================================ 
void SpectroManagerTask::set_entrance_mirror_axial_i()
  throw (Tango::DevFailed)
{
  CHECK_HW_PTR;

  SimpleCommand_resp_t resp = m_spectro_itf->entrance_mirror_move_axial();

  if (resp != CMD_ACK_OK)
  {
    THROW_DEVFAILED("COMMUNICATION_ERROR",
            "Reading error!",
            "SpectroManagerTask::set_entrance_mirror_axial_i");
  }

  // update last command value & date
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
    m_current_entrance_mirror_pos = MIRROR_POS_AXIAL;
  }
  yat::CurrentTime now;
  m_last_entrance_mirror_upd_date = yat::String::str_format("%02d-%02d-%04d_%02d:%02d:%02d"
      , now.day(), now.month(), now.year()
      , now.hour(), now.minute(), (int)now.second());
}

// ============================================================================
// SpectroManagerTask::set_entrance_mirror_lateral ()
// ============================================================================ 
void SpectroManagerTask::set_entrance_mirror_lateral()
  throw (Tango::DevFailed)
{
    DEBUG_STREAM << "SpectroManagerTask::set_entrance_mirror_lateral() entering... " << endl;

    yat::Message * msg = yat::Message::allocate(CMD_ENTRANCE_MIRROR_LATERAL, DEFAULT_MSG_PRIORITY, true);

    this->wait_msg_handled(msg, DEFAULT_CMD_TMO);
}

// ============================================================================
// SpectroManagerTask::set_entrance_mirror_lateral_i ()
// ============================================================================ 
void SpectroManagerTask::set_entrance_mirror_lateral_i()
  throw (Tango::DevFailed)
{
  CHECK_HW_PTR;

  SimpleCommand_resp_t resp = m_spectro_itf->entrance_mirror_move_lateral();

  if (resp != CMD_ACK_OK)
  {
    THROW_DEVFAILED("COMMUNICATION_ERROR",
            "Reading error!",
            "SpectroManagerTask::set_entrance_mirror_lateral_i");
  }

  // update last command value & date
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
    m_current_entrance_mirror_pos = MIRROR_POS_LATERAL;
  }
  yat::CurrentTime now;
  m_last_entrance_mirror_upd_date = yat::String::str_format("%02d-%02d-%04d_%02d:%02d:%02d"
      , now.day(), now.month(), now.year()
      , now.hour(), now.minute(), (int)now.second());
}

// ============================================================================
// SpectroManagerTask::set_exit_mirror_axial ()
// ============================================================================ 
void SpectroManagerTask::set_exit_mirror_axial()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "SpectroManagerTask::set_exit_mirror_axial() entering... " << endl;

  yat::Message * msg = yat::Message::allocate(CMD_EXIT_MIRROR_AXIAL, DEFAULT_MSG_PRIORITY, true);

  this->wait_msg_handled(msg, DEFAULT_CMD_TMO);
}

// ============================================================================
// SpectroManagerTask::set_exit_mirror_axial_i ()
// ============================================================================ 
void SpectroManagerTask::set_exit_mirror_axial_i()
  throw (Tango::DevFailed)
{
  CHECK_HW_PTR;

  SimpleCommand_resp_t resp = m_spectro_itf->exit_mirror_move_axial();

  if (resp != CMD_ACK_OK)
  {
    THROW_DEVFAILED("COMMUNICATION_ERROR",
          "Reading error!",
          "SpectroManagerTask::set_exit_mirror_axial_i");
  }

  // update last command value & date
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
    m_current_exit_mirror_pos = MIRROR_POS_AXIAL;
  }
  yat::CurrentTime now;
  m_last_exit_mirror_upd_date = yat::String::str_format("%02d-%02d-%04d_%02d:%02d:%02d"
      , now.day(), now.month(), now.year()
      , now.hour(), now.minute(), (int)now.second());
}

// ============================================================================
// SpectroManagerTask::set_exit_mirror_lateral ()
// ============================================================================ 
void SpectroManagerTask::set_exit_mirror_lateral()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "SpectroManagerTask::set_exit_mirror_lateral() entering... " << endl;

  yat::Message * msg = yat::Message::allocate(CMD_EXIT_MIRROR_LATERAL, DEFAULT_MSG_PRIORITY, true);

  this->wait_msg_handled(msg, DEFAULT_CMD_TMO);
}

// ============================================================================
// SpectroManagerTask::set_exit_mirror_lateral_i ()
// ============================================================================ 
void SpectroManagerTask::set_exit_mirror_lateral_i()
  throw (Tango::DevFailed)
{
  CHECK_HW_PTR;

  SimpleCommand_resp_t resp = m_spectro_itf->exit_mirror_move_lateral();

  if (resp != CMD_ACK_OK)
  {
    THROW_DEVFAILED("COMMUNICATION_ERROR",
            "Reading error!",
            "SpectroManagerTask::set_exit_mirror_lateral_i");
  }

  // update last command value & date
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
    m_current_exit_mirror_pos = MIRROR_POS_LATERAL;
  }
  yat::CurrentTime now;
  m_last_exit_mirror_upd_date = yat::String::str_format("%02d-%02d-%04d_%02d:%02d:%02d"
      , now.day(), now.month(), now.year()
      , now.hour(), now.minute(), (int)now.second());
}

// ============================================================================
// SpectroManagerTask::start_spectro_init_process_i ()
// ============================================================================ 
void SpectroManagerTask::start_spectro_init_process_i()
{
  m_error_status = "";

  if (!m_spectro_itf)
  {
    // internal error
    m_init_status = SPECTRO_INIT_ERR;
    m_error_status = "Internal error occurred while starting spectro init process!";
    return;
  }

  try
  {
    // request "where am i" to know the spectrometer internal state
    WhereAmI_resp_t resp = m_spectro_itf->where_am_i();
    yat::Message * msg = NULL;

    switch (resp)
    {
      case WHERE_AM_I_DONE:
        m_init_status = SPECTRO_INIT_NEARLY;
        // next step = finalize init
        msg = yat::Message::allocate(INIT_FINAL_NEARLY, DEFAULT_MSG_PRIORITY, true);
        this->post(msg);
        break;

      case WHERE_AM_I_INIT_MOTORS_DONE:
          m_init_status = SPECTRO_INIT_FINAL;
          // next step = finalize init
          msg = yat::Message::allocate(INIT_FINAL, DEFAULT_MSG_PRIORITY, true);
          this->post(msg);
          break;

      case WHERE_AM_I_CONT:
        m_init_status = SPECTRO_MAIN_PROG;
        // next step = start main program
        msg = yat::Message::allocate(INIT_MAIN_PROG, DEFAULT_MSG_PRIORITY, true);
        this->post(msg);
        break;

      case WHERE_AM_I_UNKNOWN:
        // no answer from spectrometer
        m_init_status = SPECTRO_HUNG; 
        m_error_status = "Spectro hung: No answer to WHERE AM I request!";
        break;
   
      case WHERE_AM_I_ERR:
      default:
        // bad answer from spectrometer
        m_init_status = SPECTRO_INIT_ERR;
        m_error_status = "Spectrometer error: bad answer to WHERE AM I request!";
        break;
    }
  }
  catch (Tango::DevFailed & df)
  {
    // Tango error
    ERROR_STREAM << df << std::endl;
    yat::OSStream oss;
    oss << "Failed to send WHERE AM I request: caugth [DevFailed]!" 
        << " -- reason: " << df.errors[0].reason 
        << " --Desc: " << df.errors[0].desc
        << " --Origin: " << df.errors[0].origin << std::endl;

    m_error_status = oss.str();
    m_init_status = SPECTRO_INIT_ERR;
  }
  catch (...)
  {
    // unknown error
    m_init_status = SPECTRO_INIT_ERR;
    m_error_status = "Internal error occurred while requesting WHERE AM I!";
  }
}

// ============================================================================
// SpectroManagerTask::start_spectro_main_prog_i ()
// ============================================================================ 
void SpectroManagerTask::start_spectro_main_prog_i()
{
  m_error_status = "";

  if (!m_spectro_itf)
  {
    // internal error
    m_init_status = SPECTRO_INIT_ERR;
    m_error_status = "Internal error occurred while starting main prog!";
    return;
  }

  try
  {
    // start main program
    StartMainProg_resp_t resp = m_spectro_itf->start_main_prog();
    yat::Message * msg = NULL;

    switch (resp)
    {
      case START_MAIN_PROG_DONE:
        m_init_status = SPECTRO_INIT;
        // next step = start init (to know if main prog is really launched)
        msg = yat::Message::allocate(INIT_START, DEFAULT_MSG_PRIORITY, true);
        this->post(msg);
        break;

      case START_MAIN_PROG_UNKNOWN:
        // no answer from spectrometer
        m_init_status = SPECTRO_INIT_ERR;
        m_error_status = "Spectro hung: No answer to START MAIN PROG request!";
        break;

      case START_MAIN_PROG_ERR:
      default:
        // bad answer from spectrometer
        m_init_status = SPECTRO_INIT_ERR;
        m_error_status = "Spectrometer error: bad answer to START MAIN PROG request!";
        break;
    }
  }
  catch (Tango::DevFailed & df)
  {
    // Tango error
    ERROR_STREAM << df << std::endl;
    yat::OSStream oss;
    oss << "Failed to send START MAIN PROG request: caugth [DevFailed]!"
        << " -- reason: " << df.errors[0].reason
        << " --Desc: " << df.errors[0].desc
        << " --Origin: " << df.errors[0].origin << std::endl;

    m_error_status = oss.str();
    m_init_status = SPECTRO_INIT_ERR;
  }
  catch (...)
  {
    // unknown error
    m_init_status = SPECTRO_INIT_ERR;
    m_error_status = "Internal error occurred while requesting START MAIN PROG!";
  }
}

// ============================================================================
// SpectroManagerTask::finalize_spectro_init_process_i ()
// ============================================================================ 
void SpectroManagerTask::finalize_spectro_init_process_i(bool final_init)
{
  m_error_status = "";

  if (!m_spectro_itf)
  {
    // internal error
    m_init_status = SPECTRO_INIT_ERR;
    m_error_status = "Internal error occurred while finalizing init process!";
    return;
  }

  // apply memorized values according to spectro init process

  // if init from boot, force to default values
  if (!final_init)
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
    m_current_grating_idx = DEFAULT_GRATING_IDX;
    m_current_entrance_mirror_pos = DEFAULT_MIRROR_POS;
    m_current_exit_mirror_pos = DEFAULT_MIRROR_POS;
  }

  if (final_init) // final init (init motors done)
  {
    // set selected grating index:
    try
    {
      yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
      set_grating_i(m_current_grating_idx);
    }
    catch (Tango::DevFailed & df)
    {
      // Tango error
      ERROR_STREAM << df << std::endl;
      yat::OSStream oss;
      oss << "Failed to set current grating: caugth [DevFailed]!"
          << " -- reason: " << df.errors[0].reason
          << " --Desc: " << df.errors[0].desc
          << " --Origin: " << df.errors[0].origin << std::endl;

      m_error_status = oss.str();
      m_init_status = SPECTRO_INIT_ERR;
      return;
    }
    catch (...)
    {
      // unknown error
      m_init_status = SPECTRO_INIT_ERR;
      m_error_status = "Internal error occurred while setting current grating!";
      return;
    }

    // wait
    yat::ThreadingUtilities::sleep(0, 100000000); // 100ms

    // set mirrors position:
    try
    {
      yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
      if (m_current_entrance_mirror_pos == MIRROR_POS_AXIAL)
      {
        set_entrance_mirror_axial_i();
      }
      else if (m_current_entrance_mirror_pos == MIRROR_POS_LATERAL)
      {
        set_entrance_mirror_lateral_i();
      }
      else
      {
        m_init_status = SPECTRO_INIT_ERR;
        m_error_status = "Invalid entrance mirror position: <" + m_current_entrance_mirror_pos + ">!";
        return;
      }
    }
    catch (Tango::DevFailed & df)
    {
      // Tango error
      ERROR_STREAM << df << std::endl;
      yat::OSStream oss;
      oss << "Failed to set current entrance mirror position: caugth [DevFailed]!"
          << " -- reason: " << df.errors[0].reason
          << " --Desc: " << df.errors[0].desc
          << " --Origin: " << df.errors[0].origin << std::endl;

      m_error_status = oss.str();
      m_init_status = SPECTRO_INIT_ERR;
      return;
    }
    catch (...)
    {
      // unknown error
      m_init_status = SPECTRO_INIT_ERR;
      m_error_status = "Internal error occurred while setting entrance mirror position!";
      return;
    }

    // wait
    yat::ThreadingUtilities::sleep(0, 100000000); // 100ms

    try
    {
      yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
      if (m_current_exit_mirror_pos == MIRROR_POS_AXIAL)
      {
        set_exit_mirror_axial_i();
      }
      else if (m_current_exit_mirror_pos == MIRROR_POS_LATERAL)
      {
        set_exit_mirror_lateral_i();
      }
      else
      {
        m_init_status = SPECTRO_INIT_ERR;
        m_error_status = "Invalid exit mirror position: <" + m_current_exit_mirror_pos + ">!";
        return;
      }
    }
    catch (Tango::DevFailed & df)
    {
      // Tango error
      ERROR_STREAM << df << std::endl;
      yat::OSStream oss;
      oss << "Failed to set current exit mirror position: caugth [DevFailed]!"
          << " -- reason: " << df.errors[0].reason
          << " --Desc: " << df.errors[0].desc
          << " --Origin: " << df.errors[0].origin << std::endl;

      m_error_status = oss.str();
      m_init_status = SPECTRO_INIT_ERR;
      return;
    }
    catch (...)
    {
      // unknown error
      m_init_status = SPECTRO_INIT_ERR;
      m_error_status = "Internal error occurred while setting exit mirror position!";
      return;
    }

    // wait
    yat::ThreadingUtilities::sleep(0, 100000000); // 100ms

    // define slit position to zero
    try
    {
      yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
      for (std::map<size_t, SlitDefinition_t>::iterator it = m_slits.begin(); it != m_slits.end(); ++it)
      {      
        SlitUpd_t cfg;
        cfg.idx = it->first;
        cfg.position = 0;
        define_slit_position_i(cfg);
        // wait
        yat::ThreadingUtilities::sleep(0, 100000000); // 100ms
      }
    }
    catch (Tango::DevFailed & df)
    {
      // Tango error
      ERROR_STREAM << df << std::endl;
      yat::OSStream oss;
      oss << "Failed to define slit position to zero: caugth [DevFailed]!"
          << " -- reason: " << df.errors[0].reason
          << " --Desc: " << df.errors[0].desc
          << " --Origin: " << df.errors[0].origin << std::endl;

      m_error_status = oss.str();
      m_init_status = SPECTRO_INIT_ERR;
      return;
    }
    catch (...)
    {
      // unknown error
      m_init_status = SPECTRO_INIT_ERR;
      m_error_status = "Internal error occurred while define slit position tio zero!";
      return;
    }

  } // end final_init

  // set init state 
  if (!final_init)
  {
    m_init_status = SPECTRO_INIT_MOTORS_NEEDED;
  }
  else
  {
    m_init_status = SPECTRO_INIT_DONE;
  }
}

// ============================================================================
// SpectroManagerTask::process_message
// ============================================================================
void SpectroManagerTask::process_message(yat::Message& msg)
  throw (Tango::DevFailed)
{
  //- handle msg
  switch (msg.type())
  {
    //- THREAD_INIT--
    case yat::TASK_INIT:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - TASK INIT" << std::endl;
    }
    break;
    //- TASK_EXIT----
    case yat::TASK_EXIT:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - TASK EXIT" << std::endl;
    }
    break;
    //- TASK_PERIODIC
    case yat::TASK_PERIODIC:
    {
      //DEBUG_STREAM << "SpectroManagerTask::process_message() - TASK PERIODIC" << std::endl;

      //- periodic activity
      this->periodic_job_i();
    }
    break;

    //- INIT_START---------
    case INIT_START:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - INIT_START" << std::endl;
      start_spectro_init_process_i();
    }
    break;
    //- INIT_FINAL---------
    case INIT_FINAL:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - INIT_FINAL" << std::endl;
      finalize_spectro_init_process_i(true);
    }
    break;
    //- INIT_MAIN_PROG---------
    case INIT_MAIN_PROG:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - INIT_MAIN_PROG" << std::endl;
      start_spectro_main_prog_i();
    }
    break;
    //- END_OF_INIT_MOTORS_TMO---------
    case END_OF_INIT_MOTORS_TMO:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - END_OF_INIT_MOTORS_TMO" << std::endl;
      // enable reading after tmo
      m_read_end_of_init_motors = true;
    }
    break;
    //- INIT_FINAL_NEARLY---------
    case INIT_FINAL_NEARLY:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - INIT_FINAL_NEARLY" << std::endl;
      finalize_spectro_init_process_i(false);
    }
    break;

    //- CMD_INIT_MOTORS---------
    case CMD_INIT_MOTORS:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - CMD_INIT_MOTORS" << std::endl;
      init_motors_i();
    }
    break;
    //- CMD_RESET---------
    case CMD_RESET:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - CMD_RESET" << std::endl;
      reset_i();
    }
    break;
    //- CMD_ABORT---------
    case CMD_ABORT:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - CMD_ABORT" << std::endl;
      abort_i();
    }
    break;
    //- CMD_ENTRANCE_MIRROR_AXIAL---------
    case CMD_ENTRANCE_MIRROR_AXIAL:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - CMD_ENTRANCE_MIRROR_AXIAL" << std::endl;
      set_entrance_mirror_axial_i();
    }
    break;
    //- CMD_ENTRANCE_MIRROR_LATERAL---------
    case CMD_ENTRANCE_MIRROR_LATERAL:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - CMD_ENTRANCE_MIRROR_LATERAL" << std::endl;
      set_entrance_mirror_lateral_i();
    }
    break;
    //- CMD_EXIT_MIRROR_AXIAL---------
    case CMD_EXIT_MIRROR_AXIAL:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - CMD_EXIT_MIRROR_AXIAL" << std::endl;
      set_exit_mirror_axial_i();
    }
    break;
    //- CMD_EXIT_MIRROR_LATERAL---------
    case CMD_EXIT_MIRROR_LATERAL:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - CMD_EXIT_MIRROR_LATERAL" << std::endl;
      set_exit_mirror_lateral_i();
    }
    break;

    //- SET_GRATING_POS---------
    case SET_GRATING_POS:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - SET_GRATING_POS" << std::endl;
      yat::int32 p = msg.get_data<yat::int32>();
      set_grating_position_i(p);
    }
    break;
    //- SET_GRATING_IDX---------
    case SET_GRATING_IDX:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - SET_GRATING_IDX" << std::endl;
      size_t i = msg.get_data<size_t>();
      set_grating_i(i);
    }
    break;
    //- DEFINE_GRATING_POS---------
    case DEFINE_GRATING_POS:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - DEFINE_GRATING_POS" << std::endl;
      yat::int32 gp = msg.get_data<yat::int32>();
      define_grating_position_i(gp);
    }
    break;
    //- DEFINE_SLIT_POS---------
    case DEFINE_SLIT_POS:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - DEFINE_SLIT_POS" << std::endl;
      SlitUpd_t cfg = msg.get_data<SlitUpd_t>();
      define_slit_position_i(cfg);
    }
    break;
    //- SET_SLIT_POS---------
    case SET_SLIT_POS:
    {
      DEBUG_STREAM << "SpectroManagerTask::process_message() - SET_SLIT_POS" << std::endl;
      SlitUpd_t cfg = msg.get_data<SlitUpd_t>();
      set_slit_position_i(cfg);
    }
    break;

    //- GET_GRATING_POS
    case GET_GRATING_POS:
    {
      //DEBUG_STREAM << "SpectroManagerTask::process_message() - GET_GRATING_POS" << std::endl;
      yat::int32 gp = get_grating_pos_i();
      msg.attach_data(gp);
    }
    break;
    //- GET_SLIT_POS
    case GET_SLIT_POS:
    {
      //DEBUG_STREAM << "SpectroManagerTask::process_message() - GET_SLIT_POS" << std::endl;
      size_t idx = msg.get_data<size_t>();
      double sp = get_slit_position_i(idx);
      msg.attach_data(sp);
    }
    break;

    //- UNHANDLED MSG
    default:
      WARN_STREAM << "SpectroManagerTask::process_message::unhandled msg type received" << std::endl;
      break;
    }
}

// ============================================================================
// SpectroManagerTask::init_motors_tmo
// ============================================================================
void SpectroManagerTask::init_motors_tmo(yat::Thread::IOArg arg)
  throw (Tango::DevFailed)
{
   DEBUG_STREAM << "SpectroManagerTask::init_motors_tmo: entering ..." << std::endl;
   yat::Message * msg = yat::Message::allocate(END_OF_INIT_MOTORS_TMO, DEFAULT_MSG_PRIORITY, true);
   this->post(msg);
}


// ============================================================================
// SpectroManagerTask::periodic_job_i ()
// ============================================================================ 
void SpectroManagerTask::periodic_job_i()
{
  //DEBUG_STREAM << "SpectroManagerTask::periodic_job_i() entering..." << std::endl;

  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
    // check if already in error
    if (m_mgr_state == Tango::FAULT)
      return;
  }

  // get GPIB device state
  std::string l_hw_device_status = "";
  Tango::DevState l_hw_device_st = Tango::UNKNOWN;
  if (!m_spectro_itf)
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
    l_hw_device_status = "Cannot access to GPIB device proxy!";
    l_hw_device_st = Tango::UNKNOWN;
    std::string main_msg = "Internal error!";
    m_mgr_state = Tango::FAULT;
    m_mgr_status = compose_mgr_status(main_msg, l_hw_device_st, l_hw_device_status);
    return;
  }
  else
  {
    l_hw_device_st = m_spectro_itf->get_state_and_status(l_hw_device_status);
  }

  // check GPIB device state
  if (l_hw_device_st != Tango::ON)
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
    std::string main_msg = "GPIB device proxy in error!";
    m_mgr_state = Tango::FAULT;
    m_mgr_status = compose_mgr_status(main_msg, l_hw_device_st, l_hw_device_status);
    return;
  }

  // check if init status in error
  if ((m_init_status == SPECTRO_INIT_ERR) ||
      (m_init_status == SPECTRO_HUNG))
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
    std::string main_msg = "Spectrometer initialization failed!";
    m_mgr_state = Tango::FAULT;
    m_mgr_status = compose_mgr_status(main_msg, l_hw_device_st, l_hw_device_status);
    return;
  }

  // check if init motors in progress
  // in this case, try to read end of init motors (and nothing else)
  if (m_init_status == SPECTRO_INIT_MOTORS)
  {
    if (!m_read_end_of_init_motors)
    {
      yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);

      m_mgr_state = Tango::RUNNING;
      std::string main_msg = "Init motors in progress...";
      m_mgr_status = compose_mgr_status(main_msg, l_hw_device_st, l_hw_device_status);
      return;
    }

    try
    {
      MotorsInit_resp_t resp = m_spectro_itf->get_end_of_init_motors();
      if (resp == MOTORS_INIT_DONE) // init motors ended
      {
        m_init_status = SPECTRO_INIT_DONE;

        // kill tmo pulser (should already be killed...)
        if (m_end_of_init_motors_pulser)
        {
          m_end_of_init_motors_pulser->stop();
          delete m_end_of_init_motors_pulser;
          m_end_of_init_motors_pulser = NULL;
        }

        yat::Message * msg = yat::Message::allocate(INIT_FINAL, DEFAULT_MSG_PRIORITY, true);
        this->post(msg);
        return;
      }
      else // error!
      {
        yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);

        m_error_status = "Error occurred while reading end of init motors: bad answer!";
        std::string main_msg = "Spectrometer init motors failed!";
        m_mgr_state = Tango::FAULT;
        m_mgr_status = compose_mgr_status(main_msg, l_hw_device_st, l_hw_device_status);
        return;
      }
    }
    catch (Tango::DevFailed & df)
    {
      yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);

      // Tango error
      ERROR_STREAM << df << std::endl;
      yat::OSStream oss;
      oss << "Failed to read end of init motors: caugth [DevFailed]!"
          << " -- reason: " << df.errors[0].reason
          << " --Desc: " << df.errors[0].desc
          << " --Origin: " << df.errors[0].origin << std::endl;
      m_error_status = oss.str();
      std::string main_msg = "Spectrometer init motors failed!";
      m_mgr_state = Tango::FAULT;
      m_mgr_status = compose_mgr_status(main_msg, l_hw_device_st, l_hw_device_status);
      return;
    }
    catch (...)
    {
      yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);

      // unknown error
      m_error_status = "Internal error occurred while reading end of init motors!";
      std::string main_msg = "Spectrometer init motors failed!";
      m_mgr_state = Tango::FAULT;
      m_mgr_status = compose_mgr_status(main_msg, l_hw_device_st, l_hw_device_status);
      return;
    }
  }

  // not in FAULT neither in init motors process, check if in init phase:
  if (m_init_status != SPECTRO_INIT_DONE)
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
    std::string main_msg = "";

    // map manager state on init status
    switch(m_init_status)
    {
      case SPECTRO_INIT:
      case SPECTRO_MAIN_PROG:
      case SPECTRO_INIT_FINAL:
      case SPECTRO_INIT_NEARLY:
        m_mgr_state = Tango::INIT;
        main_msg = "Spectrometer init in progress...";
        m_mgr_status = compose_mgr_status(main_msg, l_hw_device_st, l_hw_device_status);
        break;

      case SPECTRO_INIT_MOTORS_NEEDED:
        m_mgr_state = Tango::DISABLE;
        main_msg = "Spectrometer needs INIT MOTORS";
        m_mgr_status = compose_mgr_status(main_msg, l_hw_device_st, l_hw_device_status);
        break;

      default:
        break;
    }
    return;
  }

  // init done, evaluate manager state with spectro state
  try
  {
    // get limits reached flag
    GratingLimitStatus_resp_t resp_l = m_spectro_itf->get_grating_limit_status();

    if (resp_l == GRATING_LIMIT_NOT_REACHED)
    {
      yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
      m_limits_reached = false;
    }
    else if (resp_l == GRATING_LIMIT_REACHED)
    {
      yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
      m_limits_reached = true;
    }
    else // error!
    {
      yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);

      m_error_status = "Error occurred while reading limit reached: bad answer!";
      std::string main_msg = "Spectrometer reading error!";
      m_mgr_state = Tango::FAULT;
      m_mgr_status = compose_mgr_status(main_msg, l_hw_device_st, l_hw_device_status);
      return;
    }

    // get accessories state
    AccessoriesStatus_resp_t resp_a = m_spectro_itf->get_accessories_status();
    // get grating & slits state
    GratingSlitStatus_resp_t resp_gs = m_spectro_itf->get_grating_slits_status();

    if ((resp_a == ACCESSORIES_BUSY) || (resp_gs == GRATING_SLIT_BUSY))
    {
      yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
      std::string main_msg = "Spectrometer is moving...";
      m_mgr_state = Tango::MOVING;
      m_mgr_status = compose_mgr_status(main_msg, l_hw_device_st, l_hw_device_status);
    }
    else if ((resp_a == ACCESSORIES_STANDBY) && (resp_gs == GRATING_SLIT_STANDBY))
    {
      yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);
      std::string main_msg = "Spectrometer is up and ready";
      m_mgr_state = Tango::STANDBY;
      m_mgr_status = compose_mgr_status(main_msg, l_hw_device_st, l_hw_device_status);
    }
    else // error!
    {
      yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);

      m_error_status = "Error occurred while reading spectrometer state: bad answer!";
      std::string main_msg = "Spectrometer reading error!";
      m_mgr_state = Tango::FAULT;
      m_mgr_status = compose_mgr_status(main_msg, l_hw_device_st, l_hw_device_status);
      return;
    }
  }
  catch (Tango::DevFailed & df)
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);

    // Tango error
    ERROR_STREAM << df << std::endl;
    yat::OSStream oss;
    oss << "Failed to read spectrometer state: caugth [DevFailed]!"
        << " -- reason: " << df.errors[0].reason
        << " --Desc: " << df.errors[0].desc
        << " --Origin: " << df.errors[0].origin << std::endl;
    m_error_status = oss.str();
    std::string main_msg = "Spectrometer reading error!";
    m_mgr_state = Tango::FAULT;
    m_mgr_status = compose_mgr_status(main_msg, l_hw_device_st, l_hw_device_status);
    return;
  }
  catch (...)
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);

    // unknown error
    m_error_status = "Internal error occurred while reading spectrometer state!";
    std::string main_msg = "Spectrometer reading error!";
    m_mgr_state = Tango::FAULT;
    m_mgr_status = compose_mgr_status(main_msg, l_hw_device_st, l_hw_device_status);
    return;
  }

  // check if spectrometer moving and limits reached => abort movement!
  {
    yat::AutoMutex<> guard(SpectroManagerTask::m_data_lock);

    if (m_limits_reached && (m_mgr_state == Tango::MOVING))
    {
      WARN_STREAM << "Spectrometer is MOVING and limits reached: sending an ABORT msg!" << std::endl;
      abort(false);
    }
  }
}

// ============================================================================
// SpectroManagerTask::compose_mgr_status ()
// ============================================================================ 
std::string SpectroManagerTask::compose_mgr_status(std::string main_msg, 
                                                   Tango::DevState gpib_device_state, 
                                                   std::string gpib_device_status)
{
  /* status composition (example):
     Device is up and ready
     GPIB Device state: ON
     Last Entrance Mirror Command: AXIAL - 11/05/2020 12:50:03
     Last Exit Mirror Command: none
     Last Select Grating Command: 1200 - 11/05/2020 11:53:50

     Details:
     GPIB Device status: xxxxxx
     Error cause (if any): xxxxx
  */

  std::string status_str = main_msg;
  status_str += "\n";
  status_str += "\n";

  status_str += "GPIB Device state: ";
  status_str += Tango::DevStateName[gpib_device_state];
  status_str += "\n";

  status_str += "Last Entrance Mirror Command: ";
  status_str += m_current_entrance_mirror_pos;
  status_str += " - ";
  status_str += m_last_entrance_mirror_upd_date;
  status_str += "\n";

  status_str += "Last Exit Mirror Command: ";
  status_str += m_current_exit_mirror_pos;
  status_str += " - ";
  status_str += m_last_exit_mirror_upd_date;
  status_str += "\n";

  status_str += "Last Select Grating Command: ";
  status_str += m_gratings[m_current_grating_idx];
  status_str += " - ";
  status_str += m_last_grating_upd_date;
  status_str += "\n";

  status_str += "\n";
  
  status_str += "### Details ### \n";
  status_str += "GPIB Device status: ";
  status_str += gpib_device_status;
  status_str += "\n";
  status_str += "Error cause (if any): ";
  status_str += m_error_status;
  status_str += "\n";

  return status_str;
}

} // namespace SpectroHR460_ns

