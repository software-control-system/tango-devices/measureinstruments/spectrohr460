//=============================================================================
// HardwareInterface.cpp
//=============================================================================
// abstraction.......Implementation of the GPIB interface to access to
//                   the HR460 spectrometer
// class.............HardwareInterface 
// original author...S. MINOLLI - NEXEYA-FRANCE
//=============================================================================

//=============================================================================
// DEPENDENCIES
//=============================================================================
#include "HardwareInterface.h"
#include <yat/threading/Utilities.h>
#include <yat/utils/XString.h>

namespace SpectroHR460_ns
{

//=============================================================================
//- Check GPIB proxy
//=============================================================================
#define CHECK_GPIB_PROXY \
if (!m_gpib_proxy) \
{ \
    THROW_DEVFAILED("INTERNAL_ERROR", \
                    "Request aborted - the GPIB proxy isn't properly initialized!", \
                    "HardwareInterface::check_gpib_proxy"); \
}

//=============================================================================
// HardwareInterface::HardwareInterface
//=============================================================================
HardwareInterface::HardwareInterface (Tango::DeviceImpl * host_device, Tango::DeviceProxy * gpid_proxy, size_t retries_nb)
: yat4tango::TangoLogAdapter(host_device),
  m_retries_number(retries_nb),
  m_gpib_proxy(gpid_proxy)
{
}

//=============================================================================
// HardwareInterface::~HardwareInterface()
//=============================================================================
HardwareInterface::~HardwareInterface()
{
}

//=============================================================================
// HardwareInterface::get_state_and_status()
//=============================================================================
Tango::DevState HardwareInterface::get_state_and_status(std::string& status)
{
  // get GPIB device state
  Tango::DevState l_state;
  
  if (!m_gpib_proxy)
  {
    l_state = Tango::UNKNOWN;
    status = "Cannot access to GPIB device: proxy is not defined!";
  }
  else
  {
    try
    {
      l_state = m_gpib_proxy->state();
      Tango::DeviceAttribute da = m_gpib_proxy->read_attribute("Status");
      da >> status;
    }
    catch (Tango::DevFailed &e)
    {
      l_state = Tango::UNKNOWN;
      status = "Cannot access to GPIB device state or status: " + std::string(e.errors[0].desc);
    }
    catch (...)
    {
      l_state = Tango::UNKNOWN;
      status = "Cannot access to GPIB device state or status: caught [...]!";
    }
  }
  
  return l_state;
}

//=============================================================================
// HardwareInterface::read_answer()
//=============================================================================
std::string HardwareInterface::read_answer()
  throw (Tango::DevFailed)
{
  CHECK_GPIB_PROXY;

  std::string l_ret_str;
  Tango::DeviceData l_data_ret = m_gpib_proxy->command_inout("Read");
  l_data_ret >> l_ret_str;

  DEBUG_STREAM << "Raw answer: " << l_ret_str << std::endl;
  return l_ret_str;
}

//=============================================================================
// HardwareInterface::where_am_i()
//=============================================================================
WhereAmI_resp_t HardwareInterface::where_am_i()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::where_am_i() entering..." << std::endl;
  WhereAmI_resp_t answer = WHERE_AM_I_UNKNOWN;
  
  CHECK_GPIB_PROXY;
    
  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;

  // send request
  l_request = GPIB_WHERE_AM_I_CMD;
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << GPIB_WHERE_AM_I_CMD << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  // 1st wait
  yat::ThreadingUtilities::sleep(1, 0);

  // manage retries
  size_t l_attemps = m_retries_number;
  do
  {    
    // get answer
    l_ret_str = read_answer();

    // answer interpretation
    if (strncmp(l_ret_str.c_str(), &GPIB_WHERE_AM_I_RESP_B, 1) == 0)
    {
      answer = WHERE_AM_I_CONT;
      l_attemps = 0;
    }
    else if (strncmp(l_ret_str.c_str(), &GPIB_WHERE_AM_I_RESP_S, 1) == 0)
    {
      answer = WHERE_AM_I_DONE;
      l_attemps = 0;
    }
    else if (strncmp(l_ret_str.c_str(), &GPIB_WHERE_AM_I_RESP_F, 1) == 0)
    {
      answer = WHERE_AM_I_DONE;
      l_attemps = 0;      
    }
    else if (strncmp(l_ret_str.c_str(), &GPIB_WHERE_AM_I_RESP_F2, 1) == 0)
    {
      answer = WHERE_AM_I_INIT_MOTORS_DONE;
      l_attemps = 0;      
    }
    else
    {
      ERROR_STREAM << "WHERE AM I: received wrong answer!" << std::endl;
      answer = WHERE_AM_I_ERR;
      l_attemps = 0;
    }

    if (l_attemps)
    {
      // wait before retrying
      yat::ThreadingUtilities::sleep(0, 50000000); // 50ms
      DEBUG_STREAM << "retry..." << m_retries_number - l_attemps << std::endl;
    }
  } while (l_attemps != 0);

  return answer;
}

//=============================================================================
// HardwareInterface::start_main_prog()
//=============================================================================
StartMainProg_resp_t HardwareInterface::start_main_prog()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::start_main_prog() entering..." << std::endl;
  StartMainProg_resp_t answer = START_MAIN_PROG_UNKNOWN;
  
  CHECK_GPIB_PROXY;
    
  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;

  // send request
  l_request = GPIB_START_MAIN_PROG_CMD;
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << GPIB_START_MAIN_PROG_CMD << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  l_request = "\r";
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: \r" << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  // 1st wait
  yat::ThreadingUtilities::sleep(1, 0); 

  // manage retries
  size_t l_attemps = m_retries_number;
  do
  {    
    // get answer
    l_ret_str = read_answer();

    // answer interpretation
    if (strncmp(l_ret_str.c_str(), &GPIB_START_MAIN_PROG_RESP, 1) == 0)
    {
      answer = START_MAIN_PROG_DONE;
      l_attemps = 0;
    }
    else
    {
      ERROR_STREAM << "START MAIN PROG: received wrong answer!" << std::endl;
      answer = START_MAIN_PROG_ERR;
      l_attemps = 0;
    }

    if (l_attemps)
    {
      // wait before retrying
      yat::ThreadingUtilities::sleep(0, 50000000); // 50ms
      DEBUG_STREAM << "retry..." << m_retries_number - l_attemps << std::endl;
    }
  } while (l_attemps != 0);

  return answer;
}

//=============================================================================
// HardwareInterface::motors_init()
//=============================================================================
void HardwareInterface::motors_init()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::motors_init() entering..." << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;

  // send request
  l_request = GPIB_MOTORS_INIT_CMD;
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << GPIB_MOTORS_INIT_CMD << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);
}

//=============================================================================
// HardwareInterface::get_end_of_init_motors()
//=============================================================================
MotorsInit_resp_t HardwareInterface::get_end_of_init_motors()
  throw (Tango::DevFailed)
{
  std::string l_ret_str;
  MotorsInit_resp_t answer = MOTORS_INIT_UNKNOWN;

  // get answer
  l_ret_str = read_answer();

  // answer interpretation
  if (l_ret_str.empty())
  {
    answer = MOTORS_INIT_ERR;
  }
  else if (strncmp(l_ret_str.c_str(), &GPIB_RESP_CMD_ACK, 1) == 0)
  {
    answer = MOTORS_INIT_DONE;
  }
  else
  {
    ERROR_STREAM << "MOTORS INIT: received wrong answer!" << std::endl;
    answer = MOTORS_INIT_ERR;
  }
  return answer;
}

//=============================================================================
// HardwareInterface::reboot()
//=============================================================================
void HardwareInterface::reboot()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::reboot() entering..." << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;

  // send request
  l_request = GPIB_REBOOT_CMD;
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << GPIB_REBOOT_CMD << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  // wait
  yat::ThreadingUtilities::sleep(0, 300000000); // 300ms
}

//=============================================================================
// HardwareInterface::get_grating_slits_status()
//=============================================================================
GratingSlitStatus_resp_t HardwareInterface::get_grating_slits_status()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::get_grating_slits_status() entering..." << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;
  GratingSlitStatus_resp_t answer = GRATING_SLIT_UNKNOWN;

  // send request
  l_request = GPIB_GRAT_SLIT_STATUS_CMD;
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << GPIB_GRAT_SLIT_STATUS_CMD << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  // get answer
  l_ret_str = read_answer();

  // answer interpretation
  if (strncmp(l_ret_str.c_str(), GPIB_GRAT_SLIT_STATUS_RESP_BUSY.c_str(), 2) == 0)
  {
    answer = GRATING_SLIT_BUSY;
  }
  else if (strncmp(l_ret_str.c_str(), GPIB_GRAT_SLIT_STATUS_RESP_STDB.c_str(), 2) == 0)
  {
    answer = GRATING_SLIT_STANDBY;
  }
  else
  {
    ERROR_STREAM << "GRATING SLITS STATUS: received wrong answer!" << std::endl;
    answer = GRATING_SLIT_ERR;
  }

  return answer;
}

//=============================================================================
// HardwareInterface::get_accessories_status()
//=============================================================================
AccessoriesStatus_resp_t HardwareInterface::get_accessories_status()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::get_accessories_status() entering..." << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;
  AccessoriesStatus_resp_t answer = ACCESSORIES_UNKNOWN;

  // send request
  l_request = GPIB_ACCESSORIES_STATUS_CMD;
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << GPIB_ACCESSORIES_STATUS_CMD << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  // get answer
  l_ret_str = read_answer();

  // answer interpretation
  if (strncmp(l_ret_str.c_str(), GPIB_ACCESSORIES_STATUS_RESP_BUSY.c_str(), 2) == 0)
  {
    answer = ACCESSORIES_BUSY;
  }
  else if (strncmp(l_ret_str.c_str(), GPIB_ACCESSORIES_STATUS_RESP_STDB.c_str(), 2) == 0)
  {
    answer = ACCESSORIES_STANDBY;
  }
  else
  {
    ERROR_STREAM << "ACCESSORIES STATUS: received wrong answer!" << std::endl;
    answer = ACCESSORIES_ERR;
  }

  return answer;
}

//=============================================================================
// HardwareInterface::get_grating_limit_status()
//=============================================================================
GratingLimitStatus_resp_t HardwareInterface::get_grating_limit_status()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::get_grating_limit_status() entering..." << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;
  GratingLimitStatus_resp_t answer = GRATING_LIMIT_UNKNOWN;

  // send request
  l_request = GPIB_GRATING_GET_LIMIT_STATUS_CMD;
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << GPIB_GRATING_GET_LIMIT_STATUS_CMD << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  // get answer
  l_ret_str = read_answer();

  // answer interpretation

  if (strncmp(l_ret_str.c_str(), GPIB_GRATING_GET_LIMIT_STATUS_RESP_NOT_REACHED.c_str(), 2) == 0)
  {
    answer = GRATING_LIMIT_NOT_REACHED;
  }
  else if (strncmp(l_ret_str.c_str(), &GPIB_RESP_CMD_ACK, 1) == 0)
  {
    answer = GRATING_LIMIT_REACHED;
  }
  else
  {
    ERROR_STREAM << "GRATING LIMIT STATUS: received wrong answer!" << std::endl;
    answer = GRATING_LIMIT_ERR;
  }

  return answer;
}

//=============================================================================
// HardwareInterface::grating_slits_stop()
//=============================================================================
SimpleCommand_resp_t HardwareInterface::grating_slits_stop()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::grating_slits_stop() entering..." << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;
  SimpleCommand_resp_t answer = CMD_ACK_UNKNOWN;

  // send request
  l_request = GPIB_STOP_CMD;
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << GPIB_STOP_CMD << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  // wait?
  //yat::ThreadingUtilities::sleep(0, 10000000); // 10ms

  // get answer
  l_ret_str = read_answer();

  // answer interpretation
  if (l_ret_str.empty())
  {
    answer = CMD_ACK_UNKNOWN;
  }
  else if (strncmp(l_ret_str.c_str(), &GPIB_RESP_CMD_ACK, 1) == 0)
  {
    answer = CMD_ACK_OK;
  }
  else
  {
    ERROR_STREAM << "GRATING SLITS STOP: received wrong answer!" << std::endl;
    answer = CMD_ACK_ERR;
  }
  return answer;
}

//=============================================================================
// HardwareInterface::grating_relative_move()
//=============================================================================
SimpleCommand_resp_t HardwareInterface::grating_relative_move(yat::int32 steps)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::grating_relative_move() entering..." << std::endl;
  DEBUG_STREAM << "steps = " << steps << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;
  SimpleCommand_resp_t answer = CMD_ACK_UNKNOWN;

  // send request
  std::string req = GPIB_GRATING_MOVE_CMD + std::string(",") + yat::XString<yat::int32>::to_string(steps);
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << req << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  req = GPIB_CMD_END;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  m_gpib_proxy->command_inout("Write", l_data);

  // get answer
  l_ret_str = read_answer();

  // answer interpretation
  if (l_ret_str.empty())
  {
    answer = CMD_ACK_UNKNOWN;
  }
  else if (strncmp(l_ret_str.c_str(), &GPIB_RESP_CMD_ACK, 1) == 0)
  {
    answer = CMD_ACK_OK;
  }
  else
  {
    ERROR_STREAM << "GRATING RELATIVE MOVE: received wrong answer!" << std::endl;
    answer = CMD_ACK_ERR;
  }
  return answer;
}

//=============================================================================
// HardwareInterface::grating_set_position()
//=============================================================================
SimpleCommand_resp_t HardwareInterface::grating_set_position(yat::int32 steps)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::grating_set_position() entering..." << std::endl;
  DEBUG_STREAM << "steps = " << steps << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;
  SimpleCommand_resp_t answer = CMD_ACK_UNKNOWN;

  // send request
  std::string req = GPIB_GRATING_SET_POS_CMD + std::string(",") + yat::XString<yat::int32>::to_string(steps);
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << req << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  req = GPIB_CMD_END;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  m_gpib_proxy->command_inout("Write", l_data);

  // get answer
  l_ret_str = read_answer();

  // answer interpretation
  if (l_ret_str.empty())
  {
    answer = CMD_ACK_UNKNOWN;
  }
  else if (strncmp(l_ret_str.c_str(), &GPIB_RESP_CMD_ACK, 1) == 0)
  {
    answer = CMD_ACK_OK;
  }
  else
  {
    ERROR_STREAM << "GRATING SET POSITION: received wrong answer!" << std::endl;
    answer = CMD_ACK_ERR;
  }
  return answer;
}

//=============================================================================
// HardwareInterface::get_grating_position()
//=============================================================================
SimpleCommand_resp_t HardwareInterface::get_grating_position(yat::int32 & pos)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::get_grating_position() entering..." << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;
  SimpleCommand_resp_t answer = CMD_ACK_UNKNOWN;

  // send request
  std::string req = GPIB_GRATING_GET_POS_CMD ;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << req << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  req = GPIB_CMD_END;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  m_gpib_proxy->command_inout("Write", l_data);

  // get answer
  l_ret_str = read_answer();

  // answer interpretation
  if (l_ret_str.empty())
  {
    answer = CMD_ACK_UNKNOWN;
  }
  else if (strncmp(l_ret_str.c_str(), &GPIB_RESP_CMD_ACK, 1) == 0)
  {
    answer = CMD_ACK_OK;
    // get position
    pos = yat::XString<yat::int32>::to_num(l_ret_str.substr(1,std::string::npos));
    DEBUG_STREAM << "read grating position = " << pos << " in steps" << std::endl;
  }
  else
  {
    ERROR_STREAM << "GRATING GET POSITION: received wrong answer!" << std::endl;
    answer = CMD_ACK_ERR;
  }
  return answer;
}

//=============================================================================
// HardwareInterface::slit_set_position()
//=============================================================================
SimpleCommand_resp_t HardwareInterface::slit_set_position(size_t idx, yat::int32 steps)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::slit_set_position() entering..." << std::endl;
  DEBUG_STREAM << "idx = " << idx << " - steps = " << steps << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;
  SimpleCommand_resp_t answer = CMD_ACK_UNKNOWN;

  // send request
  std::string req = GPIB_SLIT_SET_POS_CMD + std::string(",") + yat::XString<size_t>::to_string(idx) \
                    + std::string(",") + yat::XString<yat::int32>::to_string(steps);

  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << req << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  req = GPIB_CMD_END;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  m_gpib_proxy->command_inout("Write", l_data);

  // get answer
  l_ret_str = read_answer();

  // answer interpretation
  if (l_ret_str.empty())
  {
    answer = CMD_ACK_UNKNOWN;
  }
  else if (strncmp(l_ret_str.c_str(), &GPIB_RESP_CMD_ACK, 1) == 0)
  {
    answer = CMD_ACK_OK;
  }
  else
  {
    ERROR_STREAM << "SLIT SET POSITION: received wrong answer!" << std::endl;
    answer = CMD_ACK_ERR;
  }
  return answer;
}

//=============================================================================
// HardwareInterface::get_slit_position()
//=============================================================================
SimpleCommand_resp_t HardwareInterface::get_slit_position(size_t idx, yat::int32 & pos)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::get_slit_position() entering..." << std::endl;
  DEBUG_STREAM << "idx = " << idx << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;
  SimpleCommand_resp_t answer = CMD_ACK_UNKNOWN;

  // send request
  std::string req = GPIB_SLIT_GET_POS_CMD + std::string(",") + yat::XString<size_t>::to_string(idx);
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << req << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  req = GPIB_CMD_END;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  m_gpib_proxy->command_inout("Write", l_data);

  // get answer
  l_ret_str = read_answer();

  // answer interpretation
  if (l_ret_str.empty())
  {
    answer = CMD_ACK_UNKNOWN;
  }
  else if (strncmp(l_ret_str.c_str(), &GPIB_RESP_CMD_ACK, 1) == 0)
  {
    answer = CMD_ACK_OK;
    // get position
    size_t str_len = l_ret_str.size() - 2; // answer format = o<pos>\r
    pos = yat::XString<yat::int32>::to_num(l_ret_str.substr(1, str_len));
    DEBUG_STREAM << "read slit <" << idx << "> position = " << pos << " in steps" << std::endl;
  }
  else
  {
    ERROR_STREAM << "GRATING GET POSITION: received wrong answer!" << std::endl;
    answer = CMD_ACK_ERR;
  }
  return answer;
}

//=============================================================================
// HardwareInterface::slit_relative_move()
//=============================================================================
SimpleCommand_resp_t HardwareInterface::slit_relative_move(size_t idx, yat::int32 steps)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::slit_relative_move() entering..." << std::endl;
  DEBUG_STREAM << "idx = " << idx << " - steps = " << steps << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;
  SimpleCommand_resp_t answer = CMD_ACK_UNKNOWN;

  // send request
  std::string req = GPIB_SLIT_MOVE_CMD + std::string(",") + yat::XString<size_t>::to_string(idx) \
        + std::string(",") + yat::XString<yat::int32>::to_string(steps);

  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << req << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  req = GPIB_CMD_END;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  m_gpib_proxy->command_inout("Write", l_data);

  // get answer
  l_ret_str = read_answer();

  // answer interpretation
  if (l_ret_str.empty())
  {
    answer = CMD_ACK_UNKNOWN;
  }
  else if (strncmp(l_ret_str.c_str(), &GPIB_RESP_CMD_ACK, 1) == 0)
  {
    answer = CMD_ACK_OK;
  }
  else
  {
    ERROR_STREAM << "SLIT RELATIVE MOVE: received wrong answer!" << std::endl;
    answer = CMD_ACK_ERR;
  }
  return answer;
}

//=============================================================================
// HardwareInterface::turret_move_to_position()
//=============================================================================
SimpleCommand_resp_t HardwareInterface::turret_move_to_position(size_t idx)
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::turret_move_to_position() entering..." << std::endl;
  DEBUG_STREAM << "idx = " << idx << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;
  SimpleCommand_resp_t answer = CMD_ACK_UNKNOWN;

  // send request
  std::string req = "";
  if (idx == 0)
  {
    req = GPIB_TURRET_MOVE_0_CMD;
  }
  else if (idx == 1)
  {
    req = GPIB_TURRET_MOVE_1_CMD;
  }
  else
  {
    THROW_DEVFAILED(
          "DATA_OUT_OF_RANGE",
          "Bad turret position index!",
          "HardwareInterface::turret_move_to_position");
  }

  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << req << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  req = GPIB_CMD_END;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  m_gpib_proxy->command_inout("Write", l_data);

  // get answer
  l_ret_str = read_answer();

  // answer interpretation
  if (l_ret_str.empty())
  {
    answer = CMD_ACK_UNKNOWN;
  }
  else if (strncmp(l_ret_str.c_str(), &GPIB_RESP_CMD_ACK, 1) == 0)
  {
    answer = CMD_ACK_OK;
  }
  else
  {
    ERROR_STREAM << "TURRET MOVE TO POSITION: received wrong answer!" << std::endl;
    answer = CMD_ACK_ERR;
  }
  return answer;
}

//=============================================================================
// HardwareInterface::entrance_mirror_move_lateral()
//=============================================================================
SimpleCommand_resp_t HardwareInterface::entrance_mirror_move_lateral()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::entrance_mirror_move_lateral() entering..." << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;
  SimpleCommand_resp_t answer = CMD_ACK_UNKNOWN;

  // send request
  std::string req = GPIB_ENTRANCE_MIRROR_MOVE_LAT_CMD;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << req << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  req = GPIB_CMD_END;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  m_gpib_proxy->command_inout("Write", l_data);

  // get answer
  l_ret_str = read_answer();

  // answer interpretation
  if (l_ret_str.empty())
  {
    answer = CMD_ACK_UNKNOWN;
  }
  else if (strncmp(l_ret_str.c_str(), &GPIB_RESP_CMD_ACK, 1) == 0)
  {
    answer = CMD_ACK_OK;
  }
  else
  {
    ERROR_STREAM << "ENTRANCE MIRROR MOVE TO LATERAL: received wrong answer!" << std::endl;
    answer = CMD_ACK_ERR;
  }
  return answer;
}

//=============================================================================
// HardwareInterface::entrance_mirror_move_axial()
//=============================================================================
SimpleCommand_resp_t HardwareInterface::entrance_mirror_move_axial()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::entrance_mirror_move_axial() entering..." << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;
  SimpleCommand_resp_t answer = CMD_ACK_UNKNOWN;

  // send request
  std::string req = GPIB_ENTRANCE_MIRROR_MOVE_AX_CMD;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << req << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  req = GPIB_CMD_END;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  m_gpib_proxy->command_inout("Write", l_data);

  // get answer
  l_ret_str = read_answer();

  // answer interpretation
  if (l_ret_str.empty())
  {
    answer = CMD_ACK_UNKNOWN;
  }
  else if (strncmp(l_ret_str.c_str(), &GPIB_RESP_CMD_ACK, 1) == 0)
  {
    answer = CMD_ACK_OK;
  }
  else
  {
    ERROR_STREAM << "ENTRANCE MIRROR MOVE TO AXIAL: received wrong answer!" << std::endl;
    answer = CMD_ACK_ERR;
  }
  return answer;
}

//=============================================================================
// HardwareInterface::exit_mirror_move_lateral()
//=============================================================================
SimpleCommand_resp_t HardwareInterface::exit_mirror_move_lateral()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::exit_mirror_move_lateral() entering..." << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;
  SimpleCommand_resp_t answer = CMD_ACK_UNKNOWN;

  // send request
  std::string req = GPIB_EXIT_MIRROR_MOVE_LAT_CMD;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << req << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  req = GPIB_CMD_END;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  m_gpib_proxy->command_inout("Write", l_data);

  // get answer
  l_ret_str = read_answer();

  // answer interpretation
  if (l_ret_str.empty())
  {
    answer = CMD_ACK_UNKNOWN;
  }
  else if (strncmp(l_ret_str.c_str(), &GPIB_RESP_CMD_ACK, 1) == 0)
  {
    answer = CMD_ACK_OK;
  }
  else
  {
    ERROR_STREAM << "EXIT MIRROR MOVE TO LATERAL: received wrong answer!" << std::endl;
    answer = CMD_ACK_ERR;
  }
  return answer;
}

//=============================================================================
// HardwareInterface::exit_mirror_move_axial()
//=============================================================================
SimpleCommand_resp_t HardwareInterface::exit_mirror_move_axial()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "HardwareInterface::exit_mirror_move_axial() entering..." << std::endl;

  CHECK_GPIB_PROXY;

  Tango::DeviceData l_data;
  Tango::DevString  l_request;
  std::string l_ret_str;
  SimpleCommand_resp_t answer = CMD_ACK_UNKNOWN;

  // send request
  std::string req = GPIB_EXIT_MIRROR_MOVE_AX_CMD ;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  DEBUG_STREAM << "Send request on spectrometer: " << req << std::endl;
  m_gpib_proxy->command_inout("Write", l_data);

  req = GPIB_CMD_END;
  l_request = const_cast<char*>(req.c_str());
  l_data << l_request;
  m_gpib_proxy->command_inout("Write", l_data);

  // get answer
  l_ret_str = read_answer();

  // answer interpretation
  if (l_ret_str.empty())
  {
    answer = CMD_ACK_UNKNOWN;
  }
  else if (strncmp(l_ret_str.c_str(), &GPIB_RESP_CMD_ACK, 1) == 0)
  {
    answer = CMD_ACK_OK;
  }
  else
  {
    ERROR_STREAM << "EXIT MIRROR MOVE TO AXIAL: received wrong answer!" << std::endl;
    answer = CMD_ACK_ERR;
  }
  return answer;
}

}
